clear all;
%close all;
clc

load polytope_car	

% define input disturbance filter
W_d = nd2sys([25],[1 10 25],10);
%W_d = nd2sys([10 0],[1 10 0.1],10);
W_d=nd2sys([1 0.05],[.1 1.1 1],.5);
W_d=5;
% define input weight
W_i=1*pi/(180);

% define input uncertainty weight
W1 = nd2sys([1/15 1],[1/200 1],.02);
W2 = nd2sys([1/15 1],[1/200 1],.02);
W3 = nd2sys([1/15 1],[1/200 1],.02);
W=daug(W1,W2,W3);

W_fr=frsp(W,logspace(-3,3));
%vplot('liv,lm',W_fr)

% define performance weights
wp_x = nd2sys([1/200 1],[1/15 1],.005);
wp_y = nd2sys([1/200 1],[1/15 1],.003);
W_p=daug(wp_x,wp_y);
%W_p = wp_y;
W_p_fr=frsp(W_p,logspace(-3,3));


% define actuator position limit
w_u1=nd2sys([1/500 1],[1/5 1],1/1600);
w_u2=nd2sys([1/500 1],[1/5 1],1/3200);
W_u = daug(w_u1,w_u1,w_u2);

% define measurement noise
W_n = daug(.1,.1,.001);


%tracking weight
W_e=nd2sys([1 150],[1/1 1.5],50);
W_ei=nd2sys([1 150],[1/1 1.5],50);
W_s=nd2sys([1 0],[1 0.01],1);
%W_e=mmult(W_e,W_e);
%Wtr = nd2sys([0.05 2.9 105.93 6.17 0.16],[1 9.19 30.80 18.83 3.95]);
% W_e_fr=frsp(W_e,logspace(-3,3));
% Win_fr=frsp(Win,logspace(-3,3));

%Ideal model 
%Tid=nd2sys(225,[1 30 225],1);
T_r=nd2sys(225,[1 30 225],1);
%T_r=nd2sys(400,[1 40 400],1);
%Tid_fr=frsp(Tid,logspace(-3,3));
%vplot('liv,lm',Win_fr,Wtr_fr,Wperf_fr,sclout(Wdist_fr,1,1500),Tid_fr)
% Construct the weighted interconnection for the LPV problem.
% Note in this example all the weights are held CONSTANT and
%  do not vary as a function of the scheduled variables.
%parameters=[6:11]*pi/180;

for i=1:length(parameters)

eval(['P=SYS_' num2str(i) ';']);

%%%%%%%%%%%%%%%%%%%%%%%%
%%%Tracking SYSICs
%%%%%%%%%%%%%%%%%%%%%%%%

systemnames         =       'W_i W_n W_d W_p W_u W T_r W_e P';
%                            [parameters(M,qbar); w is the unc output;d is the input
%                            disturbance(the model has 2 inputs!!!);sensor noise; 
%                            flap cmd is the control input, u]
inputvar            =       '[w{3}; n{3}; d{1}; r{1}; u{3}]';
outputvar           =       '[ W; W_e; W_p; W_u; W_d; W_i;P+W_n]';
input_to_W_i        =       '[r]';
input_to_W_n       =       '[n]';
input_to_W_d      =       '[d]';
input_to_W_p          =       '[ P(1:2)]';
input_to_W_u          =       '[u]';
input_to_W      =       '[u]'; 
input_to_T_r     =       '[ W_i ]';
input_to_W_e        =       '[P(3)-T_r]';
input_to_P        =       '[W_d; u+w]';

sysoutname          =       'olic';
cleanupsysic        =       'yes';
sysic

%%%%%%%%%%%%%%%%%%%%%%%%
%%%Disturbance rejection
%%%%%%%%%%%%%%%%%%%%%%%%
% systemnames         =       'Wnoise Wtr P';
% %                            [Force disturbances; measurement noises; reference signal; control inputs]
% inputvar            =   car    '[dist{3};noise{3}; psi_cmd{1};u{2}]';
% outputvar           =       '[ Wtr; psi_cmd;P+Wnoise]';
% 
% input_to_P          =       '[dist; u]';
% input_to_Wnoise     =       '[ noise ]';
% %input_to_Tid        =       '[psi_cmd]';
% input_to_Wtr        =       '[P(3)]';
% 
% sysoutname          =       'olic';
% cleanupsysic        =       'yes';
% sysic

eval(['olic_' num2str(i) '=olic;']);
end
[k21,g21,gfin21,ax21,ay21,hamx,hamy] = hinfsyn(olic_10,5,3,0.1,100,1e-8,2,[],[],1);
[k11,g11,gfin11,ax11,ay11,hamx,hamy] = hinfsyn(olic_11,5,3,0.1,100,1e-8,2,[],[],1);
[k1,g1,gfin1,ax1,ay1,hamx,hamy] = hinfsyn(olic_12,5,3,0.1,100,1e-8,2,[],[],1);


[a1,b1,c1,d1]=unpck(olic_10);
ol_1=ss(a1,b1,c1,d1);
[a11,b11,c11,d11]=unpck(olic_11);
ol_11=ss(a11,b11,c11,d11);
[a21,b21,c21,d21]=unpck(olic_12);
ol_21=ss(a21,b21,c21,d21);

[K1,CL1,GAM1,INFO] = hinfsyn(ol_1,5,3,'METHOD','lmi');
damp(K1)
GAM1
[K11,CL11,GAM11,INFO] = hinfsyn(ol_11,5,3,'METHOD','lmi');
damp(K11)
GAM11
[K21,CL21,GAM21,INFO] = hinfsyn(ol_21,5,3,'METHOD','lmi');
damp(K21)
GAM21
K1=minreal(K1);
K11=minreal(K11);
K21=minreal(K21);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Closed loop tf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
W_i=1;
W_n=daug(1,1,1);
W_d=1;
W_p=1;
W_u=daug(1,1,1);
W=daug(1,1,1);
W_e=1;
P=SYS_11;
K11_p=pck(K11.a,K11.b,K11.c,K11.d);

systemnames         =       'W_i W_n W_d W_p W_u W T_r W_e P K11_p';
%                            [parameters(M,qbar); w is the unc output;d is the input
%                            disturbance(the model has 2 inputs!!!);sensor noise; 
%                            flap cmd is the control input, u]
inputvar            =       '[w{3}; n{3}; d{1}; r{1}]';
outputvar           =       '[ W; W_e; W_p; W_u;P]';
input_to_W_i        =       '[r]';
input_to_W_n       =       '[n]';
input_to_W_d      =       '[d]';
input_to_W_p          =       '[ P(2)]';
input_to_W_u          =       '[K11_p]';
input_to_W      =       '[K11_p]'; 
input_to_T_r     =       '[ W_i ]';
input_to_W_e        =       '[P(3)-T_r]';
input_to_P        =       '[W_d; K11_p]';
input_to_K11_p        =       '[W_d;W_i; P+W_n]';

sysoutname          =       'clic_11';
cleanupsysic        =       'yes';
sysic


[a,b,c,d]=unpck(clic_11);
clic_11=ss(a,b,c,d);
%close all;
figure(100)
bode(clic_11(4,8),logspace(-3,3))
title('Closed loop input->output')
legend('error')
hold on
figure(101)
bode(clic_11(11,8),logspace(-3,3))
legend('reference tr.')
title('Closed loop input->output')
hold on
figure(102)
bode(clic_11(6:8,8),logspace(-3,3))
title('ref.->ctrl')
hold on
figure(103)
bode(clic_11(9:11,7),logspace(-3,3))
title('disturbance att.')
hold on

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[a1,b1,c1,d1]=unpck(SYS_1);
car_1=ss(a1,b1,c1,d1);
[a11,b11,c11,d11]=unpck(SYS_11);
car_11=ss(a11,b11,c11,d11);
[a21,b21,c21,d21]=unpck(SYS_21);
car_21=ss(a21,b21,c21,d21);

t=[0 0.25 0.5 0.75 1.0 1.25]*5;
psi_cmd=[0 0 1 1 0 0]*0.1;

u=[t',psi_cmd'];
%%%%%%%%%%%%%%%%%%%%%%
options = simset('MaxStep',0.001,'MinStep',1e-6);
tic
%simulation 
[t,x,y]= sim('Hinf_CAR_d_TEST',[6.25],options,u);
te=toc
figure(110)
plot(t,cmd,'y',t,car_1(:,3),'b--',t,car_11(:,3),'r--',t,car_21(:,3),'g--')