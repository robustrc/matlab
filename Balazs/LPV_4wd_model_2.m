function y = LPV_4wd_model_2(u)

global CONSTANTS

%------------------------------------------------------------------------
% Nonlinear four wheel vehicle model. A RWD vehicle is modeled
% Created under Matlab R13, v 6.5 use Simulink v 5.0
% Created by:      B. Kulcsar   (kulcsar@aem.umn.edu)
% Created:         Jan 17, 2007
% Last modified:   Jan 17, 2007
% 
% Notations:(in car body access)
%       Longitudinal forces    
%          Fxfl - longitudinal front force on left side
%          Fxfr - longitudinal front force on right side 
%          Fxrl - longitudinal rear force on left side
%          Fxrr - longitudinal rear force on right side
%       Cornering (lateral) forces
%          Fyfl - cornering front force on left side
%          Fyfr - cornering front force on right side 
%          Fyrl - cornering rear force on left side
%          Fyrr - cornering rear force on right side
%       States
%          xdot   - CG velocity in x direction     
%          ydot   - CG velocity in y direction     
%          psidot - CG yaw rate 
%
%   The function input u contains all forces in longitudinal and lateral
%   direction, the actual value of the states and parameter.
%   Control inputs
%    u(1:3)=[Fxrl Fxrr Fyfl=Fyfr];
%   Disturbance
%    u(4:7)=[Fxfl Fxfr Fyrl Fyrr];    
%   Parameter
%    u(8)=psidot;
%   States
%    u(9:11)=[xdot ydot psidot];    
%       
%   The outputs y are the state derivatives.
%    y=[xddot yddot psiddot];
%
%   The model is a quasi LPV model, in input affine analytic form.
%------------------------------------------------------------------------
%---------------
%Input selection
%---------------
%Control inputs 
%   left and right rear wheel breaks (forces longitudinal direction)
%   Longitudinal
    Fxrl=u(1);
    Fxrr=u(2);
%   left an right front wheel due to driving 
%   Lateral (keeping the same force on both of the front wheel)
%   This means using only a single control input for the driven wheels
    Fyfl=u(3)/2;
    Fyfr=u(3)/2;
%replacement and definition of the control inputs, altogether 4, ub means u bar
    ub(1)=Fxrl;
    ub(2)=Fxrr;
    ub(3)=Fyfl;
    ub(4)=Fyfr;
%---------------
%Disturbances to be rejected
%   left and right rear wheel breaks (forces lateral direction)
%   Lateral 
    Fyrl=u(6);
    Fyrr=u(7);
%   left and right front wheel forces in longitudinal direction
%   Longitudinal
    Fxfl=u(4);
    Fxfr=u(5);
%replacement and definition of the control inputs, altogether 4, d means
%disturbance
    d(1)=Fxfl;
    d(2)=Fxfr;
    d(3)=Fyrl;
    d(4)=Fyrr;
%Now we have the d vector containing all control input elements    
ub=[ub';d'];
%---------------    
%Parameters (in two steps just to clearly view it)
    psidot=u(8); %scheduled parameter, means it is a state as well
%replacement and definition of the rhos, altogether 5
    rho=psidot;
%Now we have the rho vector containing all parameters elements    

%---------------
%States input
    xdot=u(9);
    ydot=u(10);
    psidot=u(11);
%replacement and definition of the state vector, altogether 3
    x(1)=xdot;
    x(2)=ydot;
    x(3)=psidot;
%Now we have the rho vector containing all parameters elements

%---------------
%Nonlinear equation of motion
%---------------
%xdot_dot = ( 1/m )*( m*ydot*psidot + cos(d)*( Flfl + Flfr ) - sin(d)*( Fcfl + Fcfr ) + Flrl + Flrr );
%ydot_dot = ( 1/m )*( -m*xdot*psidot + sin(d)*( Flfl + Flfr ) + cos(d)*( Fcfl + Fcfr ) + Fcrl + Fcrr );
%psidot_dot = ( 1/I )*( a*(sin(d)*(Flfl + Flfr) + cos(d)*( Fcfl + Fcfr )) + b*( Fcrl + Fcrr ) + c*(sin(d)*( Fcfl - Fcfr) + cos(d)*( Flfr - Flfl ) + Flrr - Flrl));
%---------------
%LPV equations
%---------------
%State dynamics, A(rho) matrix
%The strucure is not complicated since A(rho)=A1*rho1
%General parameter dependent map is the following:
%A0=zeros(3);
A1=[0 1 0;-1 0 0;0 0 0];
A=A1*rho;
%---------------
%Control input direction, B matrix
%---------------
B=[[1/CONSTANTS.m 1/CONSTANTS.m 0 0 1/CONSTANTS.m 1/CONSTANTS.m 0 0];...
   [0 0 1/CONSTANTS.m 1/CONSTANTS.m 0 0 1/CONSTANTS.m 1/CONSTANTS.m];...
   [-CONSTANTS.c/CONSTANTS.I CONSTANTS.c/CONSTANTS.I CONSTANTS.a/CONSTANTS.I...
   CONSTANTS.a/CONSTANTS.I -CONSTANTS.c/CONSTANTS.I CONSTANTS.c/CONSTANTS.I...
   -CONSTANTS.b/CONSTANTS.I -CONSTANTS.b/CONSTANTS.I]];
%---------------
%---------------
%State derivations and block outputs
%---------------
y=[];
y=A*x'+B*ub;
%End of the file
%---------------