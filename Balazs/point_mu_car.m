clear all;
close all;
clc

load polytop_car2	

% define input uncertainty weight

Win1 = tf([1/15 1]*0.1,[1/200 1]);
Win2 = tf([1/15 1]*0.1,[1/200 1]);
Win3 = tf([1/15 1]*0.1,[1/200 1]);
Win=blkdiag(Win1,Win2,Win3);
Win_fr=frd(Win,logspace(-3,3));
%vplot('liv,lm',Win_fr)

% define performance weights
wp_x = tf([1/200 1]*.05,[1/15 1]);
wp_y = tf([1/200 1]*0.2,[1/15 1]);
Wperf = blkdiag(wp_x,wp_y);
Wperf_fr=frd(Wperf,logspace(-3,3));


%define actuator model
G =blkdiag(1/1500,1/1500,1/3000);


% define actuator position limit
Wact = blkdiag(1/100,1/100,1/100);

% define measurement noise
Wnoise = blkdiag(.01,.01,.001);

% weight on input to turbulence model
Wdist = tf([20]*.1/1500,[1 20]);
Wdist_fr =frd(Wdist,logspace(-3,3));
%Wdist = daug(wdist);

%tracking weight
Wtr = tf([1 100]*0.1,[1/.1 1]);
Wtr = tf([0.05 2.9 105.93 6.17 0.16],[1 9.19 30.80 18.83 3.95]);
Wtr_fr=frd(Wtr,logspace(-3,3));
Win_fr=frd(Win,logspace(-3,3));

%Ideal model 
%Tid=nd2sys(225,[1 30 225],1);
Tid=tf(400,[1 40 400]);
Tid_fr=frd(Tid,logspace(-3,3));
%vplot('liv,lm',Win_fr,Wtr_fr,Wperf_fr,sclout(Wdist_fr,1,1500),Tid_fr)
% Construct the weighted interconnection for the LPV problem.
% Note in this example all the weights are held CONSTANT and
%  do not vary as a function of the scheduled variables.
%parameters=[6:11]*pi/180;

delta_front = ultidyn('inputunc_front',[1 1]);
delta_rear = ultidyn('inputunc_rear',[2 2]);
Delta=blkdiag(delta_rear,delta_front);

for i=1:length(parameters)

eval(['P=SYS_' num2str(i) ';']);
[ac,bc,cc,dc]=unpck(P);
P=ss(ac,bc,cc,dc);
%%%%%%%%%%%%%%%%%%%%%%%%
%%%Tracking SYSICs
%%%%%%%%%%%%%%%%%%%%%%%%

systemnames         =       'Win Wact Wdist G Wperf Wnoise Tid Wtr P Delta';
%                            [parameters(M,qbar); w is the unc output;d is the input
%                            disturbance(the model has 2 inputs!!!);sensor noise; 
%                            flap cmd is the control input, u]
inputvar            =       '[ dist{1}; noise{3}; psi_cmd{1};u{3}]';
outputvar           =       '[ Win; Wtr; Wperf; Wact; psi_cmd;P+Wnoise]';
input_to_Win        =       '[u]';
input_to_Wact       =       '[G]';
input_to_Delta       =      '[Win]';
input_to_Wdist      =       '[dist]';
input_to_G          =       '[ u + Delta ]';
input_to_P          =       '[Wdist; G]';
input_to_Wperf      =       '[P(1:2)]'; 
input_to_Wnoise     =       '[ noise ]';
input_to_Tid        =       '[psi_cmd]';
input_to_Wtr        =       '[Tid-P(3)]';

sysoutname          =       'olic';
cleanupsysic        =       'yes';
sysic

%%%%%%%%%%%%%%%%%%%%%%%%
%%%Disturbance rejection
%%%%%%%%%%%%%%%%%%%%%%%%
% systemnames         =       'Wnoise Wtr P';
% %                            [Force disturbances; measurement noises; reference signal; control inputs]
% inputvar            =   car    '[dist{3};noise{3}; psi_cmd{1};u{2}]';
% outputvar           =       '[ Wtr; psi_cmd;P+Wnoise]';
% 
% input_to_P          =       '[dist; u]';
% input_to_Wnoise     =       '[ noise ]';
% %input_to_Tid        =       '[psi_cmd]';
% input_to_Wtr        =       '[P(3)]';
% 
% sysoutname          =       'olic';
% cleanupsysic        =       'yes';
% sysic

eval(['olic_' num2str(i) '=olic;']);
end

% [k21,g21,gfin21,ax21,ay21,hamx,hamy] = hinfsyn(olic_21,4,3,0.1,100,1e-2,2,[],[],1);
% [k11,g11,gfin11,ax11,ay11,hamx,hamy] = hinfsyn(olic_11,4,3,0.1,100,1e-2,2,[],[],1);
% [k1,g1,gfin1,ax1,ay1,hamx,hamy] = hinfsyn(olic_1,4,3,0.1,100,1e-2,2,[],[],1);
% [k21,g21,gfin21,ax21,ay21,hamx,hamy] = hinfsyn(olic_21,4,3,0.1,1000,1e-2,2,[],[],1);
% [k11,g11,gfin11,ax11,ay11,hamx,hamy] = hinfsyn(olic_11,4,3,0.1,1000,1e-2,2,[],[],1);
% [k1,g1,gfin1,ax1,ay1,hamx,hamy] = hinfsyn(olic_1,4,3,0.1,1000,1e-2,2,[],[],1);


[K1,CL1,GAM1,INFO] = hinfsyn(olic_1.NominalValue,4,3);
GAM1
[K11,CL11,GAM11,INFO] = hinfsyn(olic_11.NominalValue,4,3);
GAM11
[K21,CL21,GAM21,INFO] = hinfsyn(olic_21.NominalValue,4,3);
GAM21
opt = dkitopt('DisplayWhileAutoIter','on','NumberOfAutoIterations',5,'AutoScalingOrder',5);
[dk_long,ginf,bnd,dkinfo] = dksyn(olic_1,4,3,opt);

GAM1
[K11,CL11,GAM11,INFO] = musyn(olic_11,4,3,opt);
GAM11
[K21,CL21,GAM21,INFO] = musyn(olic_21,4,3,opt);
GAM21

K1=minreal(K1);
K11=minreal(K11);
K21=minreal(K21);




t=[0 0.25 0.5 0.75 1.0 1.25]*5;
psi_cmd=[0 0 1 1 0 0]*0.1;

u=[t',psi_cmd'];
%%%%%%%%%%%%%%%%%%%%%%
options = simset('MaxStep',0.001,'MinStep',1e-6);
tic
%simulation 
[t,x,y]= sim('Hinf_CAR_TEST',[6.25],options,u);
te=toc



