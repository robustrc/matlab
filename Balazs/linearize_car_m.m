


clear all;
close all;
clc

X0=[21;0;0];
Y0=[21;0;0];
U0=[0;0;0];
IX=[1 2 3];
IU=[];
IY=[1 2 3];
[Xt,Ut,Yt,DXt]=trim('LINEARIZE_CAR',X0,U0,Y0,IX,IU,IY);

[A2,B2,C2,D2] = linmod('LINEARIZE_CAR', Xt, Ut);