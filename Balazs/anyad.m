


clear all;
close all;
clc
global CONSTANTS %Car geometry and mass/inertia
global int_matrix_f alpha_f slip_f %front wheel
global int_matrix_r alpha_r %rear wheel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% DEFINING THE MODEL CONSTANTS %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CONSTANTS.m = 1533;    % Vehicle mass [kg]
CONSTANTS.g = 9.81;    % Gravitational constant [m/s^2]
CONSTANTS.I = 2712;    % Vehicle inertia [kgm^2]
CONSTANTS.Jw = 1.2;     % Wheel inertia [kgm^2]                  CHECK THIS VALUE!!!!
CONSTANTS.bw = 4.6*CONSTANTS.Jw/100;     % Wheel friction coefficient
CONSTANTS.r = .3;      % Wheel radius [m]
% Front- and rear-wheel distances from c.g., respectively
CONSTANTS.a = 1.04;    % [m]
CONSTANTS.b = 1.65;    % [m]
CONSTANTS.c = 1;       % [m]   distance of the wheel from the longitudinal axis

CONSTANTS.a = 1.432;    % [m]
CONSTANTS.b = 1.472;    % [m]
CONSTANTS.c = 0.8125;       % [m]
% Front and rear normal tire forces  (CHECK THE CORRECTNESS OF THIS!!!!!)
CONSTANTS.Fzfl = CONSTANTS.b*CONSTANTS.m*CONSTANTS.g/2/(CONSTANTS.a+CONSTANTS.b);   % [N]
CONSTANTS.Fzfr = CONSTANTS.b*CONSTANTS.m*CONSTANTS.g/2/(CONSTANTS.a+CONSTANTS.b);   % [N]
CONSTANTS.Fzrl = CONSTANTS.a*CONSTANTS.m*CONSTANTS.g/2/(CONSTANTS.a+CONSTANTS.b);   % [N]
CONSTANTS.Fzrr = CONSTANTS.a*CONSTANTS.m*CONSTANTS.g/2/(CONSTANTS.a+CONSTANTS.b);   % [N]

CONSTANTS.mu=0.3;  


parameters=(-10:1:10)*pi/180;
for i=1:length(parameters)
X0=[21;0;parameters(i)];
Y0=[21;0;parameters(i)];
U0=[0;0;0;0];
IX=[1 2 3];
IU=[];
IY=[1 2 3];
%[Xt,Ut,Yt,DXt]=trim('LINEARIZE_CAR',X0,U0,Y0,IX,IU,IY);

[A,B,C,D] = linmod('LINEARIZE_CAR', X0, U0);
eval(['SYS_' num2str(i) '=pck(A,B,C,D);']);
eval(['seesys(SYS_' num2str(i) ')']) 
i
end
save polytop_car2 SYS_* parameters