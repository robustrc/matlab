function [delta_f,s_rl,s_rr,V]=StatMap_INV(Fy_f,Fx_rl,Fx_rr,vx,vy,psi_dot,s_fl,s_fr)
% Static Map Inversion 
% 
% Balint Vanek 22.1.2007
global CONSTANTS %Car geometry and mass/inertia
% global int_matrix_f alpha_f slip_f %front wheel
% global int_matrix_r alpha_r %rear wheel
%load LPV_CAR.mat

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% delta_f  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vx_fl=vx-CONSTANTS.c*psi_dot;
vy_fl=vy+CONSTANTS.a*psi_dot;

vx_fr=vx+CONSTANTS.c*psi_dot;
vy_fr=vy+CONSTANTS.a*psi_dot;

alpha_fl=atan2(vy_fl,vx_fl);
alpha_fr=atan2(vy_fr,vx_fr);

delta_fl=pac_front2(Fy_f/2,alpha_fl,s_fl);
delta_fr=pac_front2(Fy_f/2,alpha_fr,s_fr);

delta_f=(delta_fl+delta_fr)/2+(alpha_fl+alpha_fr)/2; % This is the geometric angle not from the zero sideforce line!!!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% s_rl     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vx_rl=vx-CONSTANTS.c*psi_dot;
vy_rl=vy-CONSTANTS.b*psi_dot;
alpha_rl=atan2(vy_rl,vx_rl);
s_rl=pac_rear(Fx_rl,alpha_rl);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% s_rr     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vx_rr=vx+CONSTANTS.c*psi_dot;
vy_rr=vy-CONSTANTS.b*psi_dot;
alpha_rr=atan2(vy_rr,vx_rr);
s_rr=pac_rear(Fx_rr,alpha_rr);
V=[vx_fl;vy_fl;vx_fr;vy_fr;vx_rl;vy_rl;vx_rr;vy_rr];