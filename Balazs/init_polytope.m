%------------------------------------------------------------------------
% Polytope model creation
% Created under Matlab R13, v 6.5 use Simulink v 5.0
% Created by:      B. Kulcsar   (kulcsar@aem.umn.edu)
% Created:         Jan 21, 2007
% Last modified:   Jan 21, 2007
% Using LMI control tool 
%------------------------------------------------------------------------
%Init
close all;
clear all;
clc;

global a b c m I 

a = 1.432;    % [m]
b = 1.472;    % [m]
c = 0.8125;       % [m]
m = 1533;;
I=2712;

%Psidot
RANGE=[-10*pi/180 10*pi/180];
RATE=[ -10*pi/180 10*pi/180];
PV = pvec('box',RANGE,RATE);

%Parameter varying systems, defined as LTISYSs look LMI tools for help
A0=zeros(3);
A1=[0 -1 0;1 0 0;0 0 0];

% %Original full force inputs
% %Giving the same control input
% %control inputs [Fyf=(Fyfr+Fyfl) Fxrl Fxrr ]
% B20=[0 1/m 1/m; 2*1/m 0 0 ; 2*b/I c/I -c/I];
% %sum of the rear lateral force is an average disturbance force
% %disturbances [Fxfl Fxfr Fyr=(Fyrl+Fyrr)]
% B10=[1/m 1/m 0;0 0 2*1/m;c/I -c/I 2*a/I];
% B11=zeros(3,3);
% B21=zeros(3,3);
% B0=[B10 B20];
% B1=[B11 B21];

%Simplified disturbance description,
%Fxfl Fxfr are neglected and treated later as an input multiplicative
%uncertainty

%Giving the same control input
%control inputs [Fyf=(Fyfr+Fyfl) Fxrl Fxrr ]
B20=[1/m 1/m 0; 0 0 1/m ;-c/I c/I a/I];
%sum of the rear lateral force is an average disturbance force
%disturbances [Fxfl Fxfr Fyr=(Fyrl+Fyrr)]
B10=[0; 1/m; -b/I];
B11=zeros(3,1);
B21=zeros(3,3);
B0=[B10 B20];
B1=[B11 B21];



%Polytope model set
parameters=[-30:3:30]*pi/180;
%figure
%hold
for i=1:length(parameters);
eval(['A_' num2str(i) '=A0+A1*' num2str(parameters(i)) ';']);
eval(['B_' num2str(i) '=B0+B1*' num2str(parameters(i)) ';' ]);
eval(['C_' num2str(i) '=eye(3);']);
eval(['D_' num2str(i) '=zeros(size(C_1,1),size(B_1,2));']);
eval(['SYS_' num2str(i) '=pck(A_' num2str(i) ',B_' num2str(i) ',C_' num2str(i) ',D_' num2str(i) ');']);
%eval(['pzmap(SYS_' num2str(i) ')'])
end
%hold

save polytope_car SYS_* parameters
%save polytope_car SYS_1 SYS_2 SYS_3 SYS_4 SYS_5 SYS_6 SYS_7 SYS_8 SYS_9 SYS_10
%save polytope_car SYS_6 SYS_7 SYS_8 SYS_9 SYS_10 SYS_11
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
break
LPV_car_umn


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


















