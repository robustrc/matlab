function delta=pac_front2(Fy,a,s)
global int_matrix_f alpha_f slip_f
if (a >= alpha_f(1)) && (a <  alpha_f(2))
if (s <= slip_f(1)) && (s >  slip_f(2))
    delta_ll=interp1(int_matrix_f{2,1,1},int_matrix_f{1,1,1},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,2,1},int_matrix_f{1,2,1},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,1,2},int_matrix_f{1,1,2},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,2,2},int_matrix_f{1,2,2},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(1) slip_f(2);slip_f(1) slip_f(2)],[alpha_f(1) alpha_f(1);alpha_f(2) alpha_f(2)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(2)) && (s >  slip_f(3))
    delta_ll=interp1(int_matrix_f{2,2,1},int_matrix_f{1,2,1},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,3,1},int_matrix_f{1,3,1},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,2,2},int_matrix_f{1,2,2},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,3,2},int_matrix_f{1,3,2},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(2) slip_f(3);slip_f(2) slip_f(3)],[alpha_f(1) alpha_f(1);alpha_f(2) alpha_f(2)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(3)) && (s >  slip_f(4))
    delta_ll=interp1(int_matrix_f{2,3,1},int_matrix_f{1,3,1},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,4,1},int_matrix_f{1,4,1},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,3,2},int_matrix_f{1,3,2},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,4,2},int_matrix_f{1,4,2},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(3) slip_f(4);slip_f(3) slip_f(4)],[alpha_f(1) alpha_f(1);alpha_f(2) alpha_f(2)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(4)) && (s >  slip_f(5))
    delta_ll=interp1(int_matrix_f{2,4,1},int_matrix_f{1,4,1},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,5,1},int_matrix_f{1,5,1},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,4,2},int_matrix_f{1,4,2},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,5,2},int_matrix_f{1,5,2},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(4) slip_f(5);slip_f(4) slip_f(5)],[alpha_f(1) alpha_f(1);alpha_f(2) alpha_f(2)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(5)) && (s >  slip_f(6))
    delta_ll=interp1(int_matrix_f{2,5,1},int_matrix_f{1,5,1},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,6,1},int_matrix_f{1,6,1},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,5,2},int_matrix_f{1,5,2},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,6,2},int_matrix_f{1,6,2},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(5) slip_f(6);slip_f(5) slip_f(6)],[alpha_f(1) alpha_f(1);alpha_f(2) alpha_f(2)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
else
 disp('Error: invalid s')
 return
end
elseif (a >= alpha_f(2)) && (a <  alpha_f(3))
if (s <= slip_f(1)) && (s >  slip_f(2))
    delta_ll=interp1(int_matrix_f{2,1,2},int_matrix_f{1,1,2},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,2,2},int_matrix_f{1,2,2},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,1,3},int_matrix_f{1,1,3},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,2,3},int_matrix_f{1,2,3},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(1) slip_f(2);slip_f(1) slip_f(2)],[alpha_f(2) alpha_f(2);alpha_f(3) alpha_f(3)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(2)) && (s >  slip_f(3))
    delta_ll=interp1(int_matrix_f{2,2,2},int_matrix_f{1,2,2},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,3,2},int_matrix_f{1,3,2},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,2,3},int_matrix_f{1,2,3},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,3,3},int_matrix_f{1,3,3},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(2) slip_f(3);slip_f(2) slip_f(3)],[alpha_f(2) alpha_f(2);alpha_f(3) alpha_f(3)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(3)) && (s >  slip_f(4))
    delta_ll=interp1(int_matrix_f{2,3,2},int_matrix_f{1,3,2},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,4,2},int_matrix_f{1,4,2},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,3,3},int_matrix_f{1,3,3},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,4,3},int_matrix_f{1,4,3},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(3) slip_f(4);slip_f(3) slip_f(4)],[alpha_f(2) alpha_f(2);alpha_f(3) alpha_f(3)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(4)) && (s >  slip_f(5))
    delta_ll=interp1(int_matrix_f{2,4,2},int_matrix_f{1,4,2},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,5,2},int_matrix_f{1,5,2},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,4,3},int_matrix_f{1,4,3},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,5,3},int_matrix_f{1,5,3},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(4) slip_f(5);slip_f(4) slip_f(5)],[alpha_f(2) alpha_f(2);alpha_f(3) alpha_f(3)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(5)) && (s >  slip_f(6))
    delta_ll=interp1(int_matrix_f{2,5,2},int_matrix_f{1,5,2},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,6,2},int_matrix_f{1,6,2},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,5,3},int_matrix_f{1,5,3},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,6,3},int_matrix_f{1,6,3},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(5) slip_f(6);slip_f(5) slip_f(6)],[alpha_f(2) alpha_f(2);alpha_f(3) alpha_f(3)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
else
 disp('Error: invalid s')
 return
end
elseif (a >= alpha_f(3)) && (a <  alpha_f(4))
if (s <= slip_f(1)) && (s >  slip_f(2))
    delta_ll=interp1(int_matrix_f{2,1,3},int_matrix_f{1,1,3},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,2,3},int_matrix_f{1,2,3},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,1,4},int_matrix_f{1,1,4},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,2,4},int_matrix_f{1,2,4},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(1) slip_f(2);slip_f(1) slip_f(2)],[alpha_f(3) alpha_f(3);alpha_f(4) alpha_f(4)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(2)) && (s >  slip_f(3))
    delta_ll=interp1(int_matrix_f{2,2,3},int_matrix_f{1,2,3},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,3,3},int_matrix_f{1,3,3},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,2,4},int_matrix_f{1,2,4},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,3,4},int_matrix_f{1,3,4},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(2) slip_f(3);slip_f(2) slip_f(3)],[alpha_f(3) alpha_f(3);alpha_f(4) alpha_f(4)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(3)) && (s >  slip_f(4))
    delta_ll=interp1(int_matrix_f{2,3,3},int_matrix_f{1,3,3},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,4,3},int_matrix_f{1,4,3},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,3,4},int_matrix_f{1,3,4},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,4,4},int_matrix_f{1,4,4},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(3) slip_f(4);slip_f(3) slip_f(4)],[alpha_f(3) alpha_f(3);alpha_f(4) alpha_f(4)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(4)) && (s >  slip_f(5))
    delta_ll=interp1(int_matrix_f{2,4,3},int_matrix_f{1,4,3},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,5,3},int_matrix_f{1,5,3},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,4,4},int_matrix_f{1,4,4},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,5,4},int_matrix_f{1,5,4},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(4) slip_f(5);slip_f(4) slip_f(5)],[alpha_f(3) alpha_f(3);alpha_f(4) alpha_f(4)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(5)) && (s >  slip_f(6))
    delta_ll=interp1(int_matrix_f{2,5,3},int_matrix_f{1,5,3},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,6,3},int_matrix_f{1,6,3},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,5,4},int_matrix_f{1,5,4},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,6,4},int_matrix_f{1,6,4},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(5) slip_f(6);slip_f(5) slip_f(6)],[alpha_f(3) alpha_f(3);alpha_f(4) alpha_f(4)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
else
 disp('Error: invalid s')
 return
end
elseif (a >= alpha_f(4)) && (a <  alpha_f(5))
if (s <= slip_f(1)) && (s >  slip_f(2))
    delta_ll=interp1(int_matrix_f{2,1,4},int_matrix_f{1,1,4},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,2,4},int_matrix_f{1,2,4},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,1,5},int_matrix_f{1,1,5},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,2,5},int_matrix_f{1,2,5},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(1) slip_f(2);slip_f(1) slip_f(2)],[alpha_f(4) alpha_f(4);alpha_f(5) alpha_f(5)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(2)) && (s >  slip_f(3))
    delta_ll=interp1(int_matrix_f{2,2,4},int_matrix_f{1,2,4},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,3,4},int_matrix_f{1,3,4},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,2,5},int_matrix_f{1,2,5},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,3,5},int_matrix_f{1,3,5},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(2) slip_f(3);slip_f(2) slip_f(3)],[alpha_f(4) alpha_f(4);alpha_f(5) alpha_f(5)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(3)) && (s >  slip_f(4))
    delta_ll=interp1(int_matrix_f{2,3,4},int_matrix_f{1,3,4},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,4,4},int_matrix_f{1,4,4},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,3,5},int_matrix_f{1,3,5},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,4,5},int_matrix_f{1,4,5},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(3) slip_f(4);slip_f(3) slip_f(4)],[alpha_f(4) alpha_f(4);alpha_f(5) alpha_f(5)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(4)) && (s >  slip_f(5))
    delta_ll=interp1(int_matrix_f{2,4,4},int_matrix_f{1,4,4},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,5,4},int_matrix_f{1,5,4},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,4,5},int_matrix_f{1,4,5},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,5,5},int_matrix_f{1,5,5},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(4) slip_f(5);slip_f(4) slip_f(5)],[alpha_f(4) alpha_f(4);alpha_f(5) alpha_f(5)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
elseif  (s <= slip_f(5)) && (s >  slip_f(6))
    delta_ll=interp1(int_matrix_f{2,5,4},int_matrix_f{1,5,4},Fy,'pchip'); %
    delta_lr=interp1(int_matrix_f{2,6,4},int_matrix_f{1,6,4},Fy,'pchip'); %
    delta_ul=interp1(int_matrix_f{2,5,5},int_matrix_f{1,5,5},Fy,'pchip'); %
    delta_ur=interp1(int_matrix_f{2,6,5},int_matrix_f{1,6,5},Fy,'pchip'); %
%     delta_lower = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
%     delta_upper = interp1([slip_f(1) slip_f(2)],[delta_upper_pchip delta_lower_pchip],s);
    delta_in    = interp2([slip_f(5) slip_f(6);slip_f(5) slip_f(6)],[alpha_f(4) alpha_f(4);alpha_f(5) alpha_f(5)],[delta_ll delta_lr;delta_ul delta_ur],s,a);
else
 disp('Error: invalid s')
 return
end
else
 disp('Error: invalid a wheel')
 disp(a)
 return
end

delta=delta_in;