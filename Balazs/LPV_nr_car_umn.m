% lpv_nr_iowa.m

% This file assumes that you have run "flutter_ic_iowa.m" prior to ti.

%load wt_ol_ic2.mat

load olic_car

% CTPRESCL is used to find a single constant D-scaling, for a single point
%  interconnection that so how balances out the differences between robustness
%  and performance. It is important that the order and meaning of the STATES for
%  all the interconnected systems remain the SAME across the scheduled variables.
%  Hence implies that the state coordinate transformation must be done to
%  every SYSTEM. Note that this scaling is often not necessary...

[tmpx,ny,nu,nx] = minfo(olic_1);


midomega = .25;    % 25 rad/sec, frequency point at which to find the constant D-scale

% TMP is the scaled interconnection
% DL is the left constant D-scale
% DR is the right constant D-scale
% midomega is the freq point used to find the constant D-scaling
%[TMP,DL,DR,midomega] = ctpresc(olic_1,[1 1;3 3],2,1,midomega);

% Rectangular grid of scheduled variables. these must be in monotonically increasing
% order or the software will crash.

psivec  = [1:10]*pi/180;
psivec=parameters;

% Setup data structure for LPV Systems
mtype = 1;					% system type, lti
niv = 1;					% # of scheduled variables, 2
ivnames = abs(str2mat('psi'))';		% name of sched variables
ivlen = [length(psivec)'];	% length of each sched variable
ivdata = [psivec];			% data for sched variables
pmflg = -1;					% all grid data is present
pmlist = [];					% grid data missing
lftdata = [1 1 nx; nu ny 1];			% LTI info for each IC
nr = nx + ny;					% # of rows
nc = nx + nu;					% # of columns
nmeas = 3;					% # of measurements for feedback
ncont = 3;					% # of actuators for control

%Key Names, these names have meaning  (???)
lftnames = abs(str2mat('ContStates','I/O'))';

% Allocates a matrix that has space for all data
%  BACTLPV - complete LPV model data structure. now it needs to be filled with data

[BACTLPV, datalength] =...
 alcmumat(nr,nc,mtype,niv,pmflg,ivlen,ivdata,ivnames,pmlist,lftdata,lftnames);

% fill the BACTLPV data structure with the weighted interconnection data.


for i = 1:ivlen
	eval(['tmp = olic_' num2str(i) ';']);
    	% scale each IC with the constant D-scales
        %tmp = sclit(tmp,DL,DR,midomega);
	[A,B,C,D] = unpck(tmp);
	% need to repack mutools data into NEW data structure with SYSPCK
	sys = syspck(A,B,C,D);
	% INSRTBIX inserts scaled, weight IC into the BACTLPV data structure.
	%  I and J correspond to the location in the scheduled parameter vectors
	%  MACHVEC and DYNVEC.
	eval(' insrtbix(BACTLPV,[i j],sys); ');
  end



clear pcval A B C D sys pmflg pmlist lftdata ivdata ivlen ivnames
clear pcvec mtype nr nc nx ny nu npts gam_bnds tmpm tmpmm npts 
clear zeroidx midomega tmp i j datalength lftnames niv tmpx str

save BACTLPV BACTLPV

getnames(BACTLPV)

% NROFSYN synthesizes a Non-Rate bounded LPV controller. The resulting LPV
% controller, defined at the SAME independent variables as BACTLPV is Ke.

gamlow = .1;
gamhigh = 10000;
gamtol = 1;
lmisol = 2; % use LMI toolboxes LMI solver.

% the [] implies start with [] as a feasible solution.

[Ke,game] = nrofsyn(BACTLPV,nmeas,ncont,gamlow,gamhigh,gamtol,[],lmisol);
break
save example_nr

getnames(Ke)

dynvec  = [125 136 150 175 200 225];
machvec = [50 70 78 82];
 
% extract the controller corresonding to the 2nd entry of IV1 (MACH), 0.70,
%  and the 5th entry of IV2 (Dynamic pressure), 200 psi.

K_machp7_dyn200 = xv2sys(Ke,[2 5]);
minfo(K_machp7_dyn200)

