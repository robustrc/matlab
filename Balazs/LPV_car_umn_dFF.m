load polytope_car	

% define input disturbance filter
W_d = nd2sys([25],[1 10 25],10);
%W_d = nd2sys([10 0],[1 10 0.1],10);
W_d=nd2sys([1 0.05],[.1 1.1 1],.5);
W_d=5;
% define input weight
W_i=1*pi/(180);

% define input uncertainty weight
W1 = nd2sys([1/15 1],[1/200 1],.02);
W2 = nd2sys([1/15 1],[1/200 1],.02);
W3 = nd2sys([1/15 1],[1/200 1],.02);
W=daug(W1,W2,W3);

W_fr=frsp(W,logspace(-3,3));
%vplot('liv,lm',W_fr)

%W_p_fr=frsp(W_p,logspace(-3,3));


% define actuator position limit


% define measurement noise
W_n = daug(.01,.01,.001);


%tracking weight
%W_s=nd2sys([1 0],[1 0.01],1);
%W_e=mmult(W_e,W_e);
%Wtr = nd2sys([0.05 2.9 105.93 6.17 0.16],[1 9.19 30.80 18.83 3.95]);
% W_e_fr=frsp(W_e,logspace(-3,3));
% Win_fr=frsp(Win,logspace(-3,3));

%Ideal model 
%Tid=nd2sys(225,[1 30 225],1);
T_r=nd2sys(225,[1 30 225],1);
%T_r=nd2sys(400,[1 40 400],1);
%Tid_fr=frsp(Tid,logspace(-3,3));
%vplot('liv,lm',Win_fr,Wtr_fr,Wperf_fr,sclout(Wdist_fr,1,1500),Tid_fr)
% Construct the weighted interconnection for the LPV problem.
% Note in this example all the weights are held CONSTANT and
%  do not vary as a function of the scheduled variables.
%parameters=[6:11]*pi/180;

%Performaces


for i=1:length(parameters)

% define performance weights 
%parameter dependent weights

ww=abs(parameters(i));

wp_x = nd2sys([1/200 1],[1/15 1],.1);
%wp_y = nd2sys([1/200 1],[1/15 1],1*.05/ww);
wp_y = nd2sys([1/200 1],[1/15 1],1);
W_p = daug(wp_x,wp_y);
W_e=nd2sys([1 150],[1/1 1.5],50*(1+2*ww));
%W_e=nd2sys([1 150],[1/1 1.5],50);
w_u1=nd2sys([1/500 1],[1/5 1],(1+2*ww)/(1600));
%w_u1=nd2sys([1/500 1],[1/5 1],1/1600);
w_u2=nd2sys([1/500 1],[1/5 1],(1+2*ww)/(3200));
%w_u2=nd2sys([1/500 1],[1/5 1],1/(3200));
W_u = daug(w_u1,w_u1,w_u2);

%end
%break

eval(['P=SYS_' num2str(i) ';']);

%%%%%%%%%%%%%%%%%%%%%%%%
%%%Tracking SYSICs
%%%%%%%%%%%%%%%%%%%%%%%%

systemnames         =       'W_i W_n W_d W_p W_u W T_r W_e P';
%                            [parameters(M,qbar); w is the unc output;d is the input
%                            disturbance(the model has 2 inputs!!!);sensor noise; 
%                            flap cmd is the control input, u]
inputvar            =       '[w{3}; n{3}; d{1}; r{1}; u{3}]';
outputvar           =       '[ W; W_e; W_p; W_u; W_d; W_i;P+W_n]';
input_to_W_i        =       '[r]';
input_to_W_n        =       '[n]';
input_to_W_d        =       '[d]';
input_to_W_p        =       '[ P(1:2)]';
input_to_W_u        =       '[u]';
input_to_W          =       '[u]'; 
input_to_T_r        =       '[ W_i ]';
input_to_W_e        =       '[P(3)-T_r]';
input_to_P          =       '[W_d; u+w]';

sysoutname          =       'olic';
cleanupsysic        =       'yes';
sysic

%%%%%%%%%%%%%%%%%%%%%%%%
%%%Disturbance rejection
%%%%%%%%%%%%%%%%%%%%%%%%
% systemnames         =       'Wnoise Wtr P';
% %                            [Force disturbances; measurement noises; reference signal; control inputs]
% inputvar            =   car    '[dist{3};noise{3}; psi_cmd{1};u{2}]';
% outputvar           =       '[ Wtr; psi_cmd;P+Wnoise]';
% 
% input_to_P          =       '[dist; u]';
% input_to_Wnoise     =       '[ noise ]';
% %input_to_Tid        =       '[psi_cmd]';
% input_to_Wtr        =       '[P(3)]';
% 
% sysoutname          =       'olic';
% cleanupsysic        =       'yes';
% sysic

eval(['olic_' num2str(i) '=olic;']);
end

save olic_car_dFF olic_* parameters

disp(' ');
disp('LPV model IC has been setup');

%  The following SYSTEMs should be in your workspace:
% %
% >> flutter_ic_iowa
% >> who
% 
% Your variables are:
% 
% ans                 olic_M70_q150       olic_M78_q225       
% olic_M50_q125       olic_M70_q175       olic_M82_q125       
% olic_M50_q136       olic_M70_q200       olic_M82_q136       
% olic_M50_q150       olic_M70_q225       olic_M82_q150       
% olic_M50_q175       olic_M78_q125       olic_M82_q175       
% olic_M50_q200       olic_M78_q136       olic_M82_q200       
% olic_M50_q225       olic_M78_q150       olic_M82_q225       
% olic_M70_q125       olic_M78_q175       
% olic_M70_q136       olic_M78_q200       
%
% they correspond to the rectangular grid:
%  machvec = [50 70 78 82], qvec = [125 136 150 175 200 225].
LPV_nr_car_umn_dFF2
