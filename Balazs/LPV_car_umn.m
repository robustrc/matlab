load polytope_car	

% define input uncertainty weight

Win1 = nd2sys([1/15 1],[1/200 1],1/1500);
Win2 = nd2sys([1/15 1],[1/200 1],1/1500);
Win3 = nd2sys([1/15 1],[1/200 1],1/3000);
Win=daug(Win1,Win2,Win3);

% define performance weights
wp_x = nd2sys([1/200 1],[1/15 1],.01);
wp_y = nd2sys([1/200 1],[1/15 1],.05);
Wperf = daug(wp_x,wp_y);


%define actuator model
G =daug(1,1,1);

% define actuator position limit
Wact = daug(1/1500,1/1500,1/3000);

% define measurement noise
Wnoise = daug(.051,.051,.0051);

% weight on disturbance model
Wdist = nd2sys([20],[1 20],.051);
%Wdist = daug(wdist);

%tracking weight
Wtr=nd2sys([1 100],[1/.1 1],0.7);

%Ideal model 
%Tid=nd2sys(225,[1 30 225],1);
Tid=nd2sys(400,[1 40 400],1);


for i=1:length(parameters)

eval(['P=SYS_' num2str(i) ';']);

%%%%%%%%%%%%%%%%%%%%%%%%
%%%Tracking SYSICs
%%%%%%%%%%%%%%%%%%%%%%%%

systemnames         =       'Win Wact Wdist G Wperf Wnoise Tid Wtr P';
%                            [parameters(M,qbar); w is the unc output;d is the input
%                            disturbance(the model has 2 inputs!!!);sensor noise; 
%                            flap cmd is the control input, u]
inputvar            =       '[w{3}; dist{1}; noise{3}; psi_cmd{1};u{3}]';
outputvar           =       '[ Win; Wtr; Wperf; Wact; psi_cmd;P+Wnoise]';
input_to_Win        =       '[u]';
input_to_Wact       =       '[G]';
input_to_Wdist      =       '[dist]';
input_to_G          =       '[ u + w ]';
input_to_P          =       '[Wdist; G]';
input_to_Wperf      =       '[P(1:2)]'; 
input_to_Wnoise     =       '[ noise ]';
input_to_Tid        =       '[psi_cmd]';
input_to_Wtr        =       '[-Tid+P(3)]';

sysoutname          =       'olic';
cleanupsysic        =       'yes';
sysic

%%%%%%%%%%%%%%%%%%%%%%%%
%%%Disturbance rejection
%%%%%%%%%%%%%%%%%%%%%%%%
% systemnames         =       'Wnoise Wtr P';
% %                            [Force disturbances; measurement noises; reference signal; control inputs]
% inputvar            =       '[dist{3};noise{3}; psi_cmd{1};u{2}]';
% outputvar           =       '[ Wtr; psi_cmd;P+Wnoise]';
% 
% input_to_P          =       '[dist; u]';
% input_to_Wnoise     =       '[ noise ]';
% %input_to_Tid        =       '[psi_cmd]';
% input_to_Wtr        =       '[P(3)]';
% 
% sysoutname          =       'olic';
% cleanupsysic        =       'yes';
% sysic

eval(['olic_' num2str(i) '=olic;']);


end
save olic_car olic_* parameters

disp(' ');
disp('LPV model IC has been setup');

%  The following SYSTEMs should be in your workspace:
% %
% >> flutter_ic_iowa
% >> who
% 
% Your variables are:
% 
% ans                 olic_M70_q150       olic_M78_q225       
% olic_M50_q125       olic_M70_q175       olic_M82_q125       
% olic_M50_q136       olic_M70_q200       olic_M82_q136       
% olic_M50_q150       olic_M70_q225       olic_M82_q150       
% olic_M50_q175       olic_M78_q125       olic_M82_q175       
% olic_M50_q200       olic_M78_q136       olic_M82_q200       
% olic_M50_q225       olic_M78_q150       olic_M82_q225       
% olic_M70_q125       olic_M78_q175       
% olic_M70_q136       olic_M78_q200       
%
% they correspond to the rectangular grid:
%  machvec = [50 70 78 82], qvec = [125 136 150 175 200 225].
LPV_nr_car_umn2