% Initialization Routine for LPV+Static Inverse Car Model
% 1: Defining vehicle parameters
% 2: Creating the lookup table for front wheels
% (delta=f(Fy,alpha_bodyframe,slip)) - uesd by pac_front2.m
% 3: Creating the lookup table for rear wheels (slip=f(Fx,alpha)) 
% - used by pac_rear.m
% 
% Balint Vanek 22.1.2007
clear all
close all
clc
global CONSTANTS %Car geometry and mass/inertia
global int_matrix_f alpha_f slip_f %front wheel
global int_matrix_r alpha_r %rear wheel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% DEFINING THE MODEL CONSTANTS %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
CONSTANTS.m = 0.4; %1533    % Vehicle mass [kg]
CONSTANTS.g = 9.81;    % Gravitational constant [m/s^2]
CONSTANTS.I = 0.7; %2712   % Vehicle inertia [kgm^2]   How was this calculated?
CONSTANTS.Jw = 4.5562e-07;     % Wheel inertia [kgm^2]                  CHECK THIS VALUE!!!!
CONSTANTS.bw = 4.6*CONSTANTS.Jw/100;     % Wheel friction coefficient 4.6 ???
CONSTANTS.r = 0.027;      % Wheel radius [m]
% Front- and rear-wheel distances from c.g., respectively

CONSTANTS.a = 0.05;    % [m]
CONSTANTS.b = 0.052;    % [m]
CONSTANTS.c = 0.0325;       % [m] distance of the wheel from the longitudinal axis

% Front and rear normal tire forces  (CHECK THE CORRECTNESS OF THIS!!!!!)
CONSTANTS.Fzfl = CONSTANTS.b*CONSTANTS.m*CONSTANTS.g/2/(CONSTANTS.a+CONSTANTS.b);   % [N]
CONSTANTS.Fzfr = CONSTANTS.b*CONSTANTS.m*CONSTANTS.g/2/(CONSTANTS.a+CONSTANTS.b);   % [N]
CONSTANTS.Fzrl = CONSTANTS.a*CONSTANTS.m*CONSTANTS.g/2/(CONSTANTS.a+CONSTANTS.b);   % [N]
CONSTANTS.Fzrr = CONSTANTS.a*CONSTANTS.m*CONSTANTS.g/2/(CONSTANTS.a+CONSTANTS.b);   % [N]

CONSTANTS.mu=0.3;   % road friction coefficient for slippery surface
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%       FRONT WHEEL                    %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%Parameter Space where the inversion is valid%%%%%%%%%

slip_f=[0 -0.5 -1 -1.5 -2 -2.5];% slip ratios (best cornering with 0 slip, but nonzero slip could occur)
alpha_f=[-10 -5 0 5 10]/180*pi; % alpha angles on front wheels measured in the X,Y BODY frame (atan(vy/vx))!!!
delta=[-10/180*pi 10/180*pi];


%%%%%%%%%%%%%%%%%%%%  Make the interpolating routine  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
F_z = CONSTANTS.Fzfl;

delta_grid = (delta(2)-delta(1))/250;
delta_space = delta(1):delta_grid:delta(2);
int_matrix_f=cell(2,length(slip_f),length(alpha_f));

for ialpha=1:length(alpha_f)
for is=1:length(slip_f)
for i=1:length(delta_space)
    [FL,FC] = TireModel(slip_f(is),delta_space(i),F_z,CONSTANTS.mu);
    Fl(i)=FL;
    Fc(i)=FC;
    Fx(i)=FL*cos(delta_space(i)+alpha_f(ialpha))-FC*sin(delta_space(i)+alpha_f(ialpha));
    Fy(i)=FL*sin(delta_space(i)+alpha_f(ialpha))+FC*cos(delta_space(i)+alpha_f(ialpha));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Finding the max and min, where the function can be inverted
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% figure(99)
% plot(delta_space,Fy,'r--')
% hold on
[maFy,iaFy] = max(Fy);
[miFy,iiFy] = min(Fy);
% figure(99)
% plot(delta_space(iaFy),Fy(iaFy),'b+',delta_space(iiFy),Fy(iiFy),'g+')


delta_max=delta_space(iiFy);
delta_min=delta_space(iaFy);
delta_diff=delta_max-delta_min;

ds=delta_diff/16;
delta_space_int=[delta_min delta_min+4*ds delta_min+6*ds delta_min+7*ds delta_min+8*ds delta_min+9*ds delta_min+10*ds...
    delta_min+12*ds delta_max];
% delta_space_int=[delta_min delta_min+2*ds delta_min+5*ds delta_min+7*ds delta_min+8*ds delta_min+9*ds delta_min+11*ds...
%     delta_min+14*ds delta_max];
int_matrix_f{1,is,ialpha}=delta_space_int;
for i=1:length(delta_space_int)
    [FL,FC] = TireModel(slip_f(is),delta_space_int(i),F_z,CONSTANTS.mu);
    Fy_int(i)=FL*sin(delta_space_int(i)+alpha_f(ialpha))+FC*cos(delta_space_int(i)+alpha_f(ialpha));
end
int_matrix_f{2,is,ialpha}=Fy_int;
% figure(100)
% plot(delta_space_int,Fy_int)
% hold on
end
end 
clear FX FY Fx Fy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%       REAR  WHEEL                    %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%Parameter Space where the inversion is valid%%%%%%%%%

s=[0 -15];                % slip ratios (best cornering with 0 slip, but nonzero slip could occur)
alpha_r=[0 0.75/180*pi 1.25/180*pi 2.5/180*pi 5/180*pi 10/180*pi];




%%%%%%%%%%%%%%%%%%%%  Make the interpolating routine  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
F_z = CONSTANTS.Fzrl;

slip_grid = (s(2)-s(1))/100;
slip_space = s(1):slip_grid:s(2);
int_matrix_r=cell(2,length(alpha_r));

for ialpha=1:length(alpha_r)


for i=1:length(slip_space)
    [FX,FY] = TireModel(slip_space(i),alpha_r(ialpha),F_z,CONSTANTS.mu);
    Fx(i)=FX;
    Fy(i)=FY;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Finding the max and min, where the function can be inverted
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[maFx,iaFx] = max(Fx);
[miFx,iiFx] = min(Fx);


slip_min=slip_space(max(iaFx,iiFx));
slip_max=slip_space(min(iaFx,iiFx));

slip_space_int=[slip_min slip_min/2 slip_min/4 slip_min/8 slip_min/16 0];
int_matrix_r{1,ialpha}=slip_space_int;
for i=1:length(slip_space_int)
    [FX,FY] = TireModel(slip_space_int(i),alpha_r(ialpha),F_z,CONSTANTS.mu);
    Fx_int(i)=FX;
end
int_matrix_r{2,ialpha}=Fx_int;
end

save LPV_CAR CONSTANTS int_matrix_f alpha_f slip_f int_matrix_r alpha_r
clear all
%%
load LPV_CAR
xdot_i=21;
ydot_i=0;
psidot_i=0;
s_fl=0;
s_fr=0;

load Knr_II Keq Kq
% switch_ctrl=0;
% open('LPV_CAR_MDL_R13_CLOSED_SIMPLE')
% K_LPV=Keq;
% % t=[0 0.25 0.5 0.75 1.0 1.25]*5;
% % psi_cmd=[0 0 1 1 0 0]*0.1;
% % 
% % 
%  options = simset('MaxStep',0.001,'MinStep',1e-6);
% % u=[t',psi_cmd'];
% % tic
% % %simulation 
% % [tsim,x,y]= sim('LPV_CAR_MDL_R13_CLOSED_SIMPLE',[8.5],options,u);
% % te=toc
% 
% 
% t=[(0:100)/100*2*pi];
% 
% psi_cmd=[(cos(t)-1)*0.15];
% psi_cmd=[sin(t)*0.15];
% t=[t*0.5 18.5];
% psi_cmd=[-psi_cmd 0];
% dist=psi_cmd*0;
% u=[t',psi_cmd',dist'];
% %u=[t',psi_cmd'];
% tic
% %simulation 
% [tsim,x,y]= sim('LPV_CAR_MDL_R13_CLOSED_SIMPLE',[8.5],options,u);
% te=toc
%return
K_LPV=Keq;
options = simset('MaxStep',0.001,'MinStep',1e-6);
% t=[(0:100)/100*2*pi];
% 
% psi_cmd=[(cos(t)-1)*0.1];
% 
% t=[t*0.5 18.5];
% psi_cmd=[-psi_cmd 0];
% dist=psi_cmd*20;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t1=[(1:100)/100*2*pi];
psi_cmd1=[(cos(t1)-1)*0.05];
t2=[(1:100)/100*pi];
psi_cmd2=[(cos(t1)-1)*0.1];

psi_cmd=[0  psi_cmd1 -psi_cmd1   -psi_cmd2     psi_cmd2              0                     0]/2;
t=      [0  t1        t1(end)+t1 2*t1(end)+t2  2*t1(end)+t2(end)+t2  2*t1(end)+2.1*t2(end) 24]/3;

%plot(t,psi_cmd)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%u=[t',psi_cmd',dist'];
u=[t',psi_cmd'];
tic
%simulation 
[tsim,x,y]= sim('LPV_CAR_MDL_R13_CLOSED_DIST',[12.5],options,u);
te=toc

%save resultsCW K_LPV time states ctrl1 psi_cmd

plotting
break
plot(time,states(:,3),'m-.')
hold
plot(time,cmd)
figure
plot(time,states(:,2))
figure
plot(time,ctrl1)
return
% t=[0 0.05 0.055 0.1 0.105 0.15 0.1505 0.2];
% Fy_f=0*ones(1,length(t))+100*[0 0 1 1 1 1 0 0];
% Fx_rl=0*ones(1,length(t))-10.0*[0 0 1 1 -1 -1 0 0];%    [M,DELTA] = LFTDATA(A)
% Fx_rr=0*ones(1,length(t))-10.0*[1 1 0 0 -1 -1 0 0];%
% 
% options = simset('MaxStep',0.001,'MinStep',1e-8);
% u=[t',Fy_f',Fx_rl',Fx_rr'];
% tic
% %simulation 
% [t,x,y]= sim('LPV_CAR_MDL_R13',[0.2],options,u);
% te=toc

%%
t1=[(1:100)/100*2*pi];
psi_cmd1=[(cos(t1)-1)*0.1];
t2=[(1:100)/100*pi];
psi_cmd2=[(cos(t1)-1)*0.2];

psi_cmd=[0 psi_cmd1 -psi_cmd1 -psi_cmd2 psi_cmd2]/2;
t=[0 t1 t1(end)+t1 2*t1(end)+t2 2*t1(end)+t2(end)+t2]/3;

%plot(t,psi_cmd)

t=[t*0.5 18.5];
psi_cmd=[-psi_cmd 0];