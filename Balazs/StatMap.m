function [Fx_fl,Fy_fl,Fx_fr,Fy_fr,Fx_rl,Fy_rl,Fx_rr,Fy_rr]=StatMap(delta_f,s_fl,s_fr,s_rl,s_rr,V)
% Static Map 
% 
% Balint Vanek 22.1.2007
% V=[vx_fl,vy_fl,vx_fr,vy_fr,vx_rl,vy_rl,vx_rr,vy_rr];
global CONSTANTS %Car geometry and mass/inertia
%load LPV_CAR.mat

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Front Wheels %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Left %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vl_fl=vy_fl*sin(delta_f)+vx_fl*cos(delta_f);
% vc_fl=vy_fl*cos(delta_f)-vx_fl*sin(delta_f);
% 
% [FL,FC] = TireModel(s_fl,atan(vc_fl/vl_fl),F_z,mu(3));
% Fx_fl=FL*cos(delta_f)-FC*sin(delta_f);
% Fy_fl=FL*sin(delta_f)+FC*cos(delta_f);
% 
% % Right %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vl_fr=vy_fr*sin(delta_f)+vx_fr*cos(delta_f);
% vc_fr=vy_fr*cos(delta_f)-vx_fr*sin(delta_f);
% 
% [FL,FC] = TireModel(s_fr,atan(vc_fr/vl_fr),F_z,mu(3));
% Fx_fr=FL*cos(delta_f)-FC*sin(delta_f);
% Fy_fr=FL*sin(delta_f)+FC*cos(delta_f);
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Rear  Wheels %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Left %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% [Fx_rl,Fy_rl] = TireModel(s_rl,atan(vy_rl/vx_rl),F_z,mu(3));
% 
% % Right %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% [Fx_rr,Fy_rr] = TireModel(s_rr,atan(vy_rr/vx_rr),F_z,mu(3));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Front Wheels %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Left %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vl_fl=V(2)*sin(delta_f)+V(1)*cos(delta_f);
vc_fl=V(2)*cos(delta_f)-V(1)*sin(delta_f);

[FL,FC] = TireModel(s_fl,atan(vc_fl/vl_fl),CONSTANTS.Fzfl,mu(3));
Fx_fl=FL*cos(delta_f)-FC*sin(delta_f);
Fy_fl=FL*sin(delta_f)+FC*cos(delta_f);

% Right %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vl_fr=V(4)*sin(delta_f)+V(3)*cos(delta_f);
vc_fr=V(4)*cos(delta_f)-V(3)*sin(delta_f);

[FL,FC] = TireModel(s_fr,atan(vc_fr/vl_fr),CONSTANTS.Fzfr,mu(3));
Fx_fr=FL*cos(delta_f)-FC*sin(delta_f);
Fy_fr=FL*sin(delta_f)+FC*cos(delta_f);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rear  Wheels %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Left %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Fx_rl,Fy_rl] = TireModel(s_rl,atan(V(6)/V(5)),CONSTANTS.Fzrl,mu(3));

% Right %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Fx_rr,Fy_rr] = TireModel(s_rr,atan(V(8)/V(7)),CONSTANTS.Fzrr,mu(3));

