function s=pac_rear(Fx,a)
global int_matrix_r alpha_r
a=abs(a);
if      (a >= alpha_r(1)) && (a <  alpha_r(2))
    s_upper_pchip=interp1(int_matrix_r{2,1},int_matrix_r{1,1},Fx,'pchip'); %slip=0%
    s_lower_pchip=interp1(int_matrix_r{2,2},int_matrix_r{1,2},Fx,'pchip'); %slip=-2.5%
    s_in = interp1([alpha_r(1) alpha_r(2)],[s_upper_pchip s_lower_pchip],a);
elseif  (a >= alpha_r(2)) && (a <  alpha_r(3))
    s_upper_pchip=interp1(int_matrix_r{2,2},int_matrix_r{1,2},Fx,'pchip'); %slip=-2.5%
    s_lower_pchip=interp1(int_matrix_r{2,3},int_matrix_r{1,3},Fx,'pchip'); %slip=-5%
    s_in = interp1([alpha_r(2) alpha_r(3)],[s_upper_pchip s_lower_pchip],a);
elseif  (a >= alpha_r(3)) && (a <  alpha_r(4))
    s_upper_pchip=interp1(int_matrix_r{2,3},int_matrix_r{1,3},Fx,'pchip'); %slip=-5%
    s_lower_pchip=interp1(int_matrix_r{2,4},int_matrix_r{1,4},Fx,'pchip'); %slip=-10%
    s_in = interp1([alpha_r(3) alpha_r(4)],[s_upper_pchip s_lower_pchip],a);
elseif  (a >= alpha_r(4)) && (a <  alpha_r(5))
    s_upper_pchip=interp1(int_matrix_r{2,4},int_matrix_r{1,4},Fx,'pchip'); %slip=-5%
    s_lower_pchip=interp1(int_matrix_r{2,5},int_matrix_r{1,5},Fx,'pchip'); %slip=-10%
    s_in = interp1([alpha_r(4) alpha_r(5)],[s_upper_pchip s_lower_pchip],a);
elseif  (a >= alpha_r(5)) && (a <  alpha_r(6))
    s_upper_pchip=interp1(int_matrix_r{2,5},int_matrix_r{1,5},Fx,'pchip'); %slip=-5%
    s_lower_pchip=interp1(int_matrix_r{2,6},int_matrix_r{1,6},Fx,'pchip'); %slip=-10%
    s_in = interp1([alpha_r(5) alpha_r(6)],[s_upper_pchip s_lower_pchip],a);
else
 disp('Error on rear wheel alpha = ')
 disp(a)
 return
end
s=s_in;