% lpv_controller_II.m
% ***************************************************************************
% 
% Second step (of 3) to design a LPV controller.
% Gridding phase: close interconnections with all plants, scale and 
%                 normalize if desired, get some info
%
% Author: Andres Marcos                          University of Minnesota
% Date  : 27-Oct-1999
% Review: Clean up for transfer to Simon Hecker at DLR  09-March-2005
%
% ***************************************************************************


clc;
clear all;
close all;
fprintf('       **********************************************************\n')
fprintf('          2 STEP LPV CONTROLLER: Gridding Phase                  \n')
fprintf('       **********************************************************\n\n\n')

zz  = 0;


% Load open loop interconnections from step 1
% ===========================================

   load olic_car_dFF                              % load the selection of weights 
                                              % and interconnection

   %load step1;                               % load the 15 plants with the
                                              % interconnection and take some
   [tmpx,ny,nu,nx] = minfo(olic_1);           % info from any of them
n_to_k=5;
k_to_p=3;

% Scaling of the plants and interconnection
% =========================================

               % ************************************************
               % NOTE:                                          *         
               %      Uncomment the following lines if it is    * 
               %      desired to scale the plants from step 1   *
               % ************************************************


%midomega = [##] ;                     % this represents the middle point of the omega 
                                       % (frequency, rad/sec) we are interested in.

%[TMP,DL,DR] = ctprescl(olicFI_plant__1,[## ##],4,1,midomega);



% Create parameter varying vectors and input necessary variables
% ==============================================================

    psivec =parameters;                          % the vectors must be monotonically 
    %Machvec  = var2;                          % increasing, one for each scheduling
                                              % variable (i.e. AoA, Mach)

    mtype   = 1;                              % type of interconnection ( 1 => system)
    niv     = 1;                              % number of parameters 
    ivnames = abs(str2mat('psi'))';  % names of parameters
    ivlen   = [length(psivec)];           
    ivdata  = [psivec'];
    pmflg   = -1;                             % this and the following are always
    pmlist  = [];                             % the same. Don't know why.
    lftdata = [1 1 nx; nu ny 1];              % finality of this is not known, I guess something for LFT formation
    nr      = nx + ny;                        % #rows    (#row of 'olicFI_' + #points VARmatrix)
    nc      = nx + nu;                        % #columns (#col of 'olicFI_' + #points VARmatrix)
    nmeas   = n_to_k;                         % number measurements to controller, ny 
    ncont   = k_to_p;                          % number of controller outputs , nu


% Key names
% =========

   lftnames = abs(str2mat('ContStates','I/O'))';    % although this is important
                                                    % the reason why is not clear

% Allocates a matrix that has space for all data
% ==============================================

   [missileLPV, datalength] = alcmumat(nr,nc,mtype,niv,pmflg,ivlen,ivdata,ivnames,pmlist,lftdata,lftnames);



% Fills the previously created structure
% ======================================


   str1 = 'olic_';
   
   for ii= 1:ivlen(1),              % Loop for AoA

       psival = psivec(ii);

           eval(['tmp = ' str1 int2str(ii) ';']);   
           %tmp       = sclit(tmp,DL,DR,midomega);           % only if we use scaling
           [A,B,C,D]  = unpck(tmp);
           sys        = syspck(A,B,C,D);
           
           [ii  psival]
           eval(' insrtbix(missileLPV,[ii],sys); ');       % insert data into matrix
                                                              % named before
       end    % for jj = 1:ivlen(2),                                                     

 
   clear alphaval Machval A B C D sys pmflg pmlist lftdata ivdata ivlen ivnames
   clear alphavec Machvec mtype nr nc nx nu npts gam_bnds tmpm tmpmm npts     
   clear zeroidx midomega tmp ii ji datalength lftnames niv tmpx str


% Normalize system with the gamma value obtained from an Hinfinity design
% =======================================================================

    p     = missileLPV;           % system interconnection structure   
    nmeas = n_to_k;               % number of inputs to controller 
    ncon  = k_to_p;               % number of controller outputs
    gmin  = 0.1;                  % lower bound on gamma
    gmax  = 1000;                   % upper bound on gamma
    tolg  = 0.1;                  % tolerance 
    nout  = ny-nmeas;             % number of output channels to be scaled 
                                  %  assuming the last channels are to the controller  

   [klm,glm,gfin] = hinfdes(p,nmeas,ncon,gmin,gmax,tolg);    % only used for scaling of the
   missileLPV     = yscale(p,1:nout,minvlft(gfin));          % ouput channels which do not go
   

     % +++++++++++++++++++++++++++++++++++==+++++++
     %  'hinfdes' is as 'hinfsyn' but it uses the +
     %    interconnection with all the plants     +
     %    while 'hinfsyn' only accepts an         +
     %    interconnection with only one plant     +
     % +++++++++++++++++++++++++++++++++++==+++++++
                                                              
                                                              
   getnames(missileLPV)           % minfo for LPV systems
   %missileLPV(26:28,242)=0;
   save missileLPV_II missileLPV


% Non-rated bounded control
% =========================

               % ************************************************
               % NOTE:                                          *         
               %      Uncomment the following lines if it is    * 
               %      desired to get a non-rate bounded control *
               %      otherwise let them as they are and go to  *
               %      the third step                            *
               % ************************************************

   [Keq,gameq,Kq,gammaq,qoptq,qopteq] = nrofsyn(missileLPV,nmeas,ncon,gmin,200,1,[],2);
   beep  % for beeping to indicate end of this script

   save Knr_II Keq* gameq*  Kq*  gammaq*  qoptq*  qopteq*   

   disp('LPV non rate bounded synthesis is done')
   disp('Run INIT_LPV_CAR.m to initializate simulation')
INIT_LPV_CAR
   
   
% % continue?
% % =========
% 
% 
%    disp(' ')
%    ok = 0; while ~ok
%      nstep = lower(input('do you want to continue to the third step? (y/n): ','s'));
%      ok    = any([nstep == 'y', nstep == 'n']);
%    end
% 
%    if nstep == 'y',
% 
%        lpv_controller_III
% 
%    elseif nstep == 'n',
% 
%    end 
   