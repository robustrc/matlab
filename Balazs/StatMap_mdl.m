function y = StatMap_mdl(u)
%function [Fx_fl,Fy_fl,Fx_fr,Fy_fr,Fx_rl,Fy_rl,Fx_rr,Fy_rr]=StatMap_mdl(delta_f,s_fl,s_fr,s_rl,s_rr,V)
% Static Map 
% 
% Balint Vanek 22.1.2007
% V=[vx_fl,vy_fl,vx_fr,vy_fr,vx_rl,vy_rl,vx_rr,vy_rr];
global CONSTANTS %Car geometry and mass/inertia
%load LPV_CAR.mat
V=u(6:13);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Front Wheels %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Left %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vl_fl=vy_fl*sin(delta_f)+vx_fl*cos(delta_f);
% vc_fl=vy_fl*cos(delta_f)-vx_fl*sin(delta_f);
% 
% [FL,FC] = TireModel(s_fl,atan(vc_fl/vl_fl),F_z,mu(3));
% Fx_fl=FL*cos(delta_f)-FC*sin(delta_f);
% Fy_fl=FL*sin(delta_f)+FC*cos(delta_f);
% 
% % Right %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% vl_fr=vy_fr*sin(delta_f)+vx_fr*cos(delta_f);
% vc_fr=vy_fr*cos(delta_f)-vx_fr*sin(delta_f);
% 
% [FL,FC] = TireModel(s_fr,atan(vc_fr/vl_fr),F_z,mu(3));
% Fx_fr=FL*cos(delta_f)-FC*sin(delta_f);
% Fy_fr=FL*sin(delta_f)+FC*cos(delta_f);
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Rear  Wheels %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Left %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% [Fx_rl,Fy_rl] = TireModel(s_rl,atan(vy_rl/vx_rl),F_z,mu(3));
% 
% % Right %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% [Fx_rr,Fy_rr] = TireModel(s_rr,atan(vy_rr/vx_rr),F_z,mu(3));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Front Wheels %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Left %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vl_fl=V(2)*sin(u(1))+V(1)*cos(u(1));
vc_fl=V(2)*cos(u(1))-V(1)*sin(u(1));

[FL,FC] = TireModel(u(2),atan(vc_fl/vl_fl),CONSTANTS.Fzfl,CONSTANTS.mu);
Fx_fl=FL*cos(u(1))-FC*sin(u(1));
Fy_fl=FL*sin(u(1))+FC*cos(u(1));

% Right %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vl_fr=V(4)*sin(u(1))+V(3)*cos(u(1));
vc_fr=V(4)*cos(u(1))-V(3)*sin(u(1));

[FL,FC] = TireModel(u(3),atan(vc_fr/vl_fr),CONSTANTS.Fzfr,CONSTANTS.mu);
Fx_fr=FL*cos(u(1))-FC*sin(u(1));
Fy_fr=FL*sin(u(1))+FC*cos(u(1));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Rear  Wheels %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Left %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Fx_rl,Fy_rl] = TireModel(u(4),atan(V(6)/V(5)),CONSTANTS.Fzrl,CONSTANTS.mu);

% Right %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Fx_rr,Fy_rr] = TireModel(u(5),atan(V(8)/V(7)),CONSTANTS.Fzrr,CONSTANTS.mu);

y=[Fx_fl,Fy_fl,Fx_fr,Fy_fr,Fx_rl,Fy_rl,Fx_rr,Fy_rr];