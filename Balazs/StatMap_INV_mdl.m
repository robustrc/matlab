function y = StatMap_INV_mdl(u)
%function y = StatMap_INV(Fy_f,Fx_rl,Fx_rr,vx,vy,psi_dot,s_fl,s_fr)
% Static Map Inversion 
% 
% Balint Vanek 22.1.2007
global CONSTANTS %Car geometry and mass/inertia
% global int_matrix_f alpha_f slip_f %front wheel
% global int_matrix_r alpha_r %rear wheel
%load LPV_CAR.mat

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% delta_f  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

vx_fl=u(4)-CONSTANTS.c*u(6);
vy_fl=u(5)+CONSTANTS.a*u(6);

vx_fr=u(4)+CONSTANTS.c*u(6);
vy_fr=u(5)+CONSTANTS.a*u(6);

alpha_fl=atan2(vy_fl,vx_fl);
alpha_fr=atan2(vy_fr,vx_fr);

delta_fl=pac_front2(u(1)/2,alpha_fl,u(7));
delta_fr=pac_front2(u(1)/2,alpha_fr,u(8));

delta_f=(delta_fl+delta_fr)/2+(alpha_fl+alpha_fr)/2; % This is the geometric angle not from the zero sideforce line!!!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% s_rl     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vx_rl=u(4)-CONSTANTS.c*u(6);
vy_rl=u(5)-CONSTANTS.b*u(6);
alpha_rl=atan2(vy_rl,vx_rl);
s_rl=pac_rear(u(2),alpha_rl);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% s_rr     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vx_rr=u(4)+CONSTANTS.c*u(6);
vy_rr=u(5)-CONSTANTS.b*u(6);
alpha_rr=atan2(vy_rr,vx_rr);
s_rr=pac_rear(u(3),alpha_rr);

V=[vx_fl;vy_fl;vx_fr;vy_fr;vx_rl;vy_rl;vx_rr;vy_rr];
y=[delta_f;s_rl;s_rr;V];