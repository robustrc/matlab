% lpv_controller_III.m
% ***************************************************************************
% 
% Third and last step in the design of a LPV controller. 
%
% Basis functions for the X and Y Riccati matrices are defined. 
% Also, bounds for the rate of the varying parameters are given. 
% Finally, the LPV control design algorithm is run.
%
%
% Author: Andres Marcos                          University of Minnesota
% Date  : 27-Oct-1999
% Review: Clean up for transfer to Simon Hecker at DLR  09-March-2005
% 
% ***************************************************************************

clear all;
close all;
clc;
fprintf('       **********************************************************\n')
fprintf('          3 STEP LPV CONTROLLER: LPV control                     \n')
fprintf('       **********************************************************\n\n\n')


% Load some info from step 1
% ==========================
% load olic_car 
%   load characteristics;



% ****************************************************************************
% 				X   BASIS FUNCTION
% ****************************************************************************


% Parameter dependent X Scaling:
% ==============================

% X(rho) = sum_{i=1}^{b1} f_i(rho)*F_i*X_i*F_i^T +
%          sum_{j=1}^{b2} g_j(rho)*[ E_j*X_j^{~}*G_j^T + G_j*X_j^{~}*E_j^T]


% X basis function creation
% =========================

  nbas = 2;                                    % let's start with three X basis 
                                               % functions: 1 constant, 1 alpha, 1 Mach 

  Xfbasis = creatbas(nbas,str2mat('psi'));
  Xfbasis = insrtbas(Xfbasis,1,'1');	       % 1st basis, a constant
  Xfbasis = insrtbas(Xfbasis,2,'psi');        % 2nd basis, linear in alpha

 
% Derivatives of X basis functions
% ================================

   % ********************************************************************* 
   % NOTE:                                                               *
   % the derivate of the X basis functions command is as follows:        * 
   % Xfbasis = insrtbp(Xfbasis,basis_func#,IV,'derivate as a string');   *
   %                                                                     *
   %         where:                                                      *
   %			basis_func# = number of the basis function       *
   %                    IV          = parameter varying number           *
   %                    'der..'     = value of the derivative in string  *
   %                                  form                               *
   %         e.g:    Xfbasis = insrtbp(Xfbasis,1,1,'0')                  *
   %                                           | |  |---> derivative     *
   %                    D (cte)                | |            is         *
   %                   ----------- = 0         | |           zero        *
   %                    d (alpha)              | |                       *
   %                                           | |-------> der. wrt      *
   %                                           |             alpha       *
   %                                           |                         *
   %                                           |---------> der. of fist  *
   %                                                         basis func. *
   %                                                       (i.e. cte)    *
   %  more examples:                                                     *
   %                % Adding 2 basis functions would look like:          *
   %                %	Xfbasis = creatbas(4,'alpha');                   *
   %                %	Xfbasis = insrtbas(Xfbasis,3,'sin(p)');          *
   %                %	Xfbasis = insrtbas(Xfbasis,4,'cos(p)');          *
   %                %	Xfbasis = insrtbp(Xfbasis,3,1,'cos(p)');         *
   %                %	Xfbasis = insrtbp(Xfbasis,4,1,'-sin(p)');        * 
   %                                                                     *
   % *********************************************************************

   % derivative of the 1st bases function wrt to 1st IndepVar: psi
   Xfbasis = insrtbp(Xfbasis,1,1,'0'); 
   % derivative of the 2nd bases function wrt to 1st IndepVar: psi
   Xfbasis = insrtbp(Xfbasis,2,1,'1');

  
   Xfmat = [];	% this shorthand means all F_i matrices are the Identity

   Xgbasis = []; Xemat = []; Xgmat = [];


% ****************************************************************************
% 				Y   BASIS FUNCTION
% ****************************************************************************


% Parameter dependent Y Scaling:
% ==============================

% Y(rho) = sum_{i=1}^{b1} f_i(rho)*F_i*Y_i*F_i^T +
%          sum_{j=1}^{b2} g_j(rho)*[ E_j*Y_j^{~}*G_j^T + G_j*Y_j^{~}*E_j^T]


% Y basis function creation
% =========================  

  nbas = 2;
  
  Yfbasis = creatbas(nbas,str2mat('psi'));
  Yfbasis = insrtbas(Yfbasis,1,'1');	         % 1st basis, a constant
  Yfbasis = insrtbas(Yfbasis,2,'psi');	         % 1st basis, a constant

% Derivatives of Y basis functions
% ================================

   Yfbasis = insrtbp(Yfbasis,1,1,'0');  % derivatives wrt alpha
   Yfbasis = insrtbp(Yfbasis,2,1,'1');

   Yfmat = [];	% this shorthand means all F_i matrices are the Identity

   Ygbasis = []; Yemat = []; Ygmat = [];

% ****************************************************************************
% 			  RATE BOUNDED VARYING PARAMETERS
% ****************************************************************************



% design a: rate bounds of +500 to -1000 ft/sec
% design b: rate bounds of +/- 2 rad/sec

pdupp = [10];		               % Upper Rate Bound: ## is the rate of change   
upprows  = str2mat('psi');	   %  in ALPHA and BETA (alpha and Mach)
                                       %  same units as grid point vectors  

pdlow = [-10];                       % Lower Rate Bound: in ALPHA and BETA
lowrows  = str2mat('psi');


%save lpv_design_tmp_pr -ascii


% ****************************************************************************
% 			       LPV synthesis
% ****************************************************************************

disp('Calling LPVOFSGB....')

% define optimization params
% ===========================

	nmeas = 3; 
	ncont = 4;
	gmin  = 0.9;	
	gmax  = 1000;	
	gtol  = 1;


% load info from step II
% ======================

	load missileLPV_II;

    
% Find controller, Kprex
% ======================

     [Kpre,gampre,Kpr,gampr,...
                Xpre,Ypre,Xpr,Ypr,qreturnpr,qoptpr,...
                perfamie,perfami,bndamie,bndami] = ...
            lpvofsyn1(missileLPV,nmeas,ncont,gmin,gmax,gtol,...
                Xfbasis,Xfmat,Xgbasis,Xemat,Xgmat,...
                Yfbasis,Yfmat,Ygbasis,Yemat,Ygmat,...
                pdupp,upprows,pdlow,lowrows,[],1);
            
            

	Kprx = svinterp(Kpr,'psidot',0);       % set rate to 0
	Kprx = elimsiv(Kprx,'psidot');         % eliminate the alphadot IV
	
	Kprex = svinterp(Kpre,'psidot',0);     % set rate to 0
	Kprex = elimsiv(Kprex,'psidot');       % eliminate the alphadot IV
        
% Save and End
% =============
    
%save lpv_des_r0

save missilelpv_des_I  Kprex Kprx Kpre gampre Kpr gampr Xpre Ypre Xpr Ypr qreturnpr qoptpr var1 var2









