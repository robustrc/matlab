clc;
clf;
close all;
clear xq_final yq_final
figure(1);
hold on;

gridX = 3/6; % Might seem whack, but in terms of meters it makes kinda sense.
gridY = 2/4; % -------||-------
global pointDist;
% plot([gridX, gridX, gridX*5], [gridY, gridY*3, gridY*3],'k','LineWidth',2);
% plot([gridX*2, gridX*2], [0, gridY*2],'k','LineWidth',2)
% plot([gridX*3, gridX*3, gridX*5], [gridY*3, gridY, gridY],'k','LineWidth',2)
% plot([gridX*4, gridX*6], [gridY*2, gridY*2],'k','LineWidth',2)
% plot([0, 0, gridX*6,gridX*6,0], [0,gridY*4,gridY*4,0,0],'k','LineWidth',2)

axis([0 3 0 2]);
set(gcf, 'Position', [100, 500, 1000, 1000*(2/3)])

% Containers for the clicked positions
x = [];
y = [];
firstRun = 1;
button = 1;
sizeOfX = 0;

%While we haven't clicked the right mouse button, continously take inputs.
while (sum(button) <= 1)
    if (firstRun)
        [x(1),y(1),button] = ginput(1);
        firstRun = 0;
    else
        %Add the new inputs to the vectors
        [x(end+1),y(end+1),button] = ginput(1);
    end
    % If we are done (pressed right mouse button)
    % Remove the last data entry (right click adds a point)
    if (button == 3)
        x(end) = [];
        y(end) = [];
        %Turn values into multiple laps to easier create loop
        pathXY = [x' y'; x' y'; x' y'; x' y'; x' y'; x' y'; x' y';];
    end
    hold on;
    plot(x(end),y(end),'ro');
end

% Interpolate the data (get moar points)
stepLengths = sqrt(sum(diff(pathXY).^2,2));
stepLengths = [0; stepLengths]; % add the starting point
cumulativeLen = cumsum(stepLengths);
pointGain = 1000;
xq = interp1(cumulativeLen,pathXY(:,1),linspace(0,cumulativeLen(end),length(pathXY)*pointGain),'spline');
yq = interp1(cumulativeLen,pathXY(:,2),linspace(0,cumulativeLen(end),length(pathXY)*pointGain),'spline');
% Only grab the middle lap to close the loop.
lastDist = 100;
for i = 1:length(xq)-1
    pointDist = sqrt((xq(i)-xq(i+1))^2 + (yq(i)-yq(i+1))^2);
    if pointDist < lastDist
        lastDist = pointDist;
    end
end
pointDist = lastDist;

div = length(pathXY)/length(x);
xq_final(1,:) = xq(2*(size(xq,2)/div):2*(size(xq,2)/div)+pointGain/10);
yq_final(1,:) = yq(2*(size(xq,2)/div):2*(size(xq,2)/div)+pointGain/10);
%plot(xq_final,yq_final,'k','LineWidth',2)
hold on
i = length(yq_final(1,:));
while  abs(xq_final(1,1) - xq_final(1,end-1)) > pointDist || ...
        abs(yq_final(1,1) -yq_final(1,end-1)) > pointDist
    xq_final(1,i) = xq(2*(size(xq,2)/div) + i);
    yq_final(1,i) = yq(2*(size(xq,2)/div) + i);
    i = i+1;
end
xq_final= xq_final(1,1:end-1);
yq_final = yq_final(1,1:end-1);
plot(xq_final,yq_final,'k','LineWidth',2)
hold on
global dataSize;
dataSize = size(xq_final,2);
trackX = zeros(1, dataSize);
trackY = zeros(1, dataSize);
trackRadius = zeros(1, dataSize);
turnCenter = zeros(2, dataSize);
trackYaw = zeros(1, dataSize);
for i = 1:dataSize
    trackX(i) = xq_final(i);
    trackY(i) = yq_final(i);
end
%Plot first and last data point to make sure the loop looks nice
% plot(trackX(1),trackY(1),'g*')
% hold on
% plot(trackX(end),trackY(end),'ro')
% Calculate the radius using three points (trackX,trackY being the middle one)
% also get the desired yaw angle of that position.
runOnce = 1;
for i = 1:dataSize
    % Fucking matlab...
    rear = mod(i-2, dataSize)+1;
    center = mod(i-1, dataSize)+1;
    front = mod(i, dataSize)+1;
    A = getEuclidDistance2D(trackX(rear),trackX(center),trackY(rear),trackY(center));
    B = getEuclidDistance2D(trackX(rear),trackX(front),trackY(rear),trackY(front));
    C = getEuclidDistance2D(trackX(center),trackX(front),trackY(center),trackY(front));
    area = getTriangleArea(trackX(rear),trackX(center),trackX(front), trackY(rear),trackY(center),trackY(front));
    
    % It's called math honey, look it up.
    trackRadius(i) = (A*B*C)/(4*area);
    yaw = atan2(trackY(front)-trackY(rear),trackX(front)-trackX(rear));
    
    yaw = yaw .* (yaw >= 0) + (yaw + 2 * pi) .* (yaw < 0);
    trackYaw(i) = (180/pi)*yaw;
    turnCenter(:,i) = calcCircle([trackX(rear),trackY(rear)],...
        [trackX(center),trackY(center)],[trackX(front),trackY(front)]);
    
end

% % Fix the last data point
% A = getEuclidDistance2D(trackX(end-1),trackX(end),trackY(end-1),trackY(end));
% B = getEuclidDistance2D(trackX(end-1),trackX(1),trackY(end-1),trackY(1));
% C = getEuclidDistance2D(trackX(end),trackX(1),trackY(end),trackY(1));
% area = getTriangleArea(trackX(end-1),trackX(end),trackX(1), trackY(end-1),trackY(end),trackY(1));
% 
% trackRadius(end) = (A*B*C)/(4*area);
% yaw = atan2(trackY(1)-trackY(end-1), trackX(1)-trackX(end-1));
% yaw = yaw .* (yaw >= 0) + (yaw + 2 * pi) .* (yaw < 0);
% trackYaw(end) = (180/pi)*yaw;
% turnCenter(:,end) = calcCircle([trackX(end-1),trackY(end-1)],...
%     [trackX(end),trackY(end)],[trackX(1),trackY(1)]);
trackX(1) = (trackX(2)+trackX(end-1))/2;
trackY(1) = (trackY(2)+trackY(end-1))/2;
trackYaw(1)= (trackYaw(2)+trackYaw(end-1))/2;
trackRadius(1) = (trackRadius(2)+trackRadius(end-1))/2;
turnCenter(1) = (turnCenter(2) + turnCenter(end-1))/2;

trackX(end) = (trackX(2)+trackX(end-1))/2;
trackY(end) = (trackY(2)+trackY(end-1))/2;
trackYaw(end)= (trackYaw(2)+trackYaw(end-1))/2;
trackRadius(end) = (trackRadius(2)+trackRadius(end-1))/2;
turnCenter(end) = (turnCenter(2) + turnCenter(end-1))/2;

% Finally store the track in some nice way.

track = [trackX; trackY; trackYaw; trackRadius];
round(track,2);
clf
% %Plot the circles created by the radiuses to verify the data (messy)
% th = 0:pi/50:2*pi;
% hold on
% for i = 1300:5:1500
%     x = trackRadius(i) * cos(th) + turnCenter(1,i);
%     y = trackRadius(i) * sin(th) + turnCenter(2,i);
%     xEnd = (((trackRadius(i)+trackRadius(i+1))/2)/max(trackRadius))*cosd(trackYaw(i)+90)  + trackX(i);
%     yEnd = (((trackRadius(i)+trackRadius(i+1))/2)/max(trackRadius))*sind(trackYaw(i)+90)  + trackY(i);
%     if (i == 1)
%         plot(x,y,'g','LineWidth',4)
%     elseif (i == size(trackX, 2))
%         plot(x,y,'r','LineWidth',2)
%     else
%         plot(x,y,'b')
%     end
%     set(gcf, 'Position', [100, 500, 1000, 1000*(2/3)])
%     plot([trackX(i), xEnd],[trackY(i), yEnd])
% end
plot(xq_final,yq_final,'k','LineWidth',2)

hold on
axis([0 3 0 2]);
set(gcf, 'Position', [100, 500, 1000, 1000*(2/3)])