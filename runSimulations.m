function runSimulations(carInitialPose,dt,plotStep)
load('data.mat')

% Prepare to run simulations

prompt = 'How many simulations do you want to run? Enter for 1 simulations. Otherwise, type the number of desired simulations: \n';
numberOfSims = input(prompt);
if size(numberOfSims) == 0
    numberOfSims = 1;
end
clf
plot(xq_final,yq_final,'k','LineWidth',2)
hold on
axis([0 3 0 2]);
set(gcf, 'Position', [100, 500, 1000, 1000*(2/3)])
disp(['Running ', num2str(numberOfSims), ' simulation(s).'])
for i = 1:numberOfSims
    % Run simulation
    sim('sim_6_states_uncertain');
    
    % Create a bunch of containers
    globalXVel = zeros(size(simout.data,1),1);
    globalYVel = zeros(size(simout.data,1),1);
    globalXPos = zeros(size(simout.data,1),1);
    globalYPos = zeros(size(simout.data,1),1);
    yawData = zeros(size(simout.data,1),1);
    index = zeros(size(simout.data,1),1);
    
    yawData(:) = mod(simout.data(:,4),360);
    index(:) = simout.data(:,6);
    % Convert the values to global frame
    
    for k = 1:size(simout.data,1)
        globalXVel(k) = sqrt(simout.data(k,1)^2 + simout.data(k,3)^2)*cosd(yawData(k));
        globalYVel(k) = sqrt(simout.data(k,1)^2 + simout.data(k,3)^2)*sind(yawData(k));
    end
    globalXPos(1) = carInitialPose(1);
    globalYPos(1) = carInitialPose(2);
    for k = 1:size(simout.data,1)-1
        globalXPos(k+1) = globalXPos(k) + globalXVel(k)*dt;
        globalYPos(k+1) = globalYPos(k) + globalYVel(k)*dt;
    end
    
    % Create vaiables for plotting (setting the axes nice and proper)
    dataPoints = size(globalXPos,1);
    maxX = max(globalXPos(1:dataPoints));
    maxY = max(globalYPos(1:dataPoints));
    minX = min(globalXPos(1:dataPoints));
    minY = min(globalYPos(1:dataPoints));
    
    car_size_half = PARAMETER.a*1.1;
    angle = 30;
    randomColorNumber =  rand(1,3);
    figure(1);
    axis([0 3 0 2]);
    set(gcf, 'Position', [100, 500, 1000, 1000*(2/3)])
    daspect([1 1 1])
%     for k=1:plotStep:dataPoints%size(globalXPos,2)
%         dotx(1) = [globalXPos(k)+car_size_half*cosd(angle+yawData(k))];
%         dotx(2) = [globalXPos(k)+car_size_half*cosd(360-angle+yawData(k))];
%         dotx(3) = [globalXPos(k)+car_size_half*cosd(180+angle+yawData(k))];
%         dotx(4) = [globalXPos(k)+car_size_half*cosd(180-angle+yawData(k))];
%         dotx(5) = dotx(1);
%         doty(1) = [globalYPos(k)+car_size_half*sind(angle+yawData(k))];
%         doty(2) = [globalYPos(k)+car_size_half*sind(360-angle+yawData(k))];
%         doty(3) = [globalYPos(k)+car_size_half*sind(180+angle+yawData(k))];
%         doty(4) = [globalYPos(k)+car_size_half*sind(180-angle+yawData(k))];
%         doty(5) = doty(1);
%         frontx(1) = dotx(1);
%         frontx(2) = dotx(2);
%         fronty(1) = doty(1);
%         fronty(2) = doty(2);
%         figure(1);
%         fill(dotx,doty,randomColorNumber);
%         hold on
%         plot(frontx,fronty,'g','Linewidth',2)
%         plot(track(1,index(k)), track(2,index(k)), 'g.')
% %         plot(track(1,(mod(floor(index(k)+2+1000*simout.data(k,1)),dataSize))+1), ...
% %             track(2,(mod(floor(index(k)+2+1000*simout.data(k,1)),dataSize))+1), ...
% %             'r.')
%         
%         %        plot(globalXPos, globalYPos,'r.');
%         %        hold on
%         %axis([-abs(minX) maxX -abs(minY)-1 maxY+1])
%         pause(dt*plotStep)
%     end
figure(1)
hold on
plot(globalXPos,globalYPos)
    if numberOfSims > 1
        for k = 1:size(simout.data,2)
            figure(k+1)
            plot(tout, simout.data(:,k))
            hold on
        end
    end
end
figure(1)
save('results')
