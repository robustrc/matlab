clear all
clf
close all
clc
% CREATE A TRACK
run('the_trackinator');
csvwrite('track.csv',track');
save('data');

%% CREATE VEHICLE MODEL
clc
clf
close all
% NOTE: If you change the order of these parameters or add any, make sure
% to do the appropriate change in the C++ code for the car_controller ros
% node as macros are made there that are very dependent on the order.
PARAMETER.m   =  ureal('m',0.195,'percent',eps);  %    Vehicle mass. [kg]
PARAMETER.a   =  ureal('a',0.051,'percent',eps);  %    Distance from front axle to COG. [m]
PARAMETER.b   =  ureal('b',0.051,'percent',eps);  %    Distance from rear axle to COG. [m]
PARAMETER.L   =  PARAMETER.a+PARAMETER.b;  %    Distance between rear and front axle. [m]
PARAMETER.L.Name = 'L';
PARAMETER.mu = 0.5; % Friction coefficient
PARAMETER.Iz  = (1/12)*PARAMETER.m*((PARAMETER.L*1.1)^2 + 0.06^2);       %  Rotational inertia Z axis.
PARAMETER.Iz.Name = 'Iz';
PARAMETER.rfw = 0.022/2;      % Radius front wheel [m]
PARAMETER.rrw = 0.027/2;      % Radius rear wheel [m]
PARAMETER.mrw = 0.003;        % Mass rear wheel
PARAMETER.mfw = 0.0025;       % Mass front wheel
PARAMETER.Iwr = (1/2)*PARAMETER.mrw^2;  % Rotational inertia wheel.
PARAMETER.Iwf = (1/2)*PARAMETER.mfw^2;

 % Lateral tire stiffness.  [NO FUCKING CLUE]
Cyr = 0.195*50;
PARAMETER.Cyr = ureal('Cyr',Cyr,'percent',eps);
PARAMETER.stiffnessRatio = ureal('stiffnessRatio',1,'percent',eps);
PARAMETER.Cyf = Cyr*PARAMETER.stiffnessRatio;%ureal('Cyf',Cyr,'percent',20);%PARAMETER.Cyr*PARAMETER.stiffnessRatio;%
PARAMETER.Cyf.Name = 'Cyf'
PARAMETER.Ck = ureal('Ck',0.01,'percent',eps);%0.01;           % Steering angle to slip ratio factor (super made up)
PARAMETER.Cd  = 0.8;          % Air resistance coefficient. [Unitless (0,1)]
PARAMETER.p   = 1.184;        % Density of air [kg/dm^3]
PARAMETER.Af  = 0.002175;     % Frontal areal of vehicle [m^2]
PARAMETER.Vx_lin = 1;       % Speed at which we linearize around [m/s]

% Note that only the linear car model is incertain since the controller is 
% based on the error model, LQR design does not support uncertain models.
[linear_car_model, error_based_linear_car_model] = createModels(PARAMETER);
nominalParameters = getNominalParameters(PARAMETER);

% CREATE CONTROLLER
% LQR controller matrices
Q = [
      1   0    0   0   0   0
      0   2000   0   0   0   0
      0   0     1e4 0   0   0
      0   0     0   1   0   0
      0   0     0   0   1e3 0
      0   0     0   0   0   1
    ];
R = [1  0
    0   1];
Q_s = [
      100   0   0   0   0
      0     10 0   0   1
      0     0   100   0   0
      0     0   0   10 0
      0     0   0   0   1
    ];
R_s = [100  0
    0   100];
%[K,~,~] = lqr(linear_car_model.NominalValue,Q_s,R_s);
%[K_error_d,~,~] = lqrd(error_based_linear_car_model.A.NominalValue ...
%    ,error_based_linear_car_model.B.NominalValue,Q,R,dt);
[K_error,~,~] = lqr(error_based_linear_car_model.NominalValue,Q,R);

dlmwrite('controller.csv',K_error);
dlmwrite('controller.csv',nominalParameters);
%%
% ANALYSE ROBUSTNESS
F_loopsens = feedback(error_based_linear_car_model,K_error);
%F_loopsens.To.InputName = {'dvelE','dintLatE','dLatE','dlatVelE','dyawE','dyawrateE'};
%F_loopsens.To.OutputName = {'velE','intLatE','LatE','latVelE','yawE','yawrateE'};

%opt = robOptions('Display','on');
%[stabmarg,wcu] = robgain(F.Si,4)
opts = robOptions('Sensitivity','On');
[stabmarg,wcu,info] = robstab(F_loopsens,opts)
info.Sensitivity
% bodemag(F.Lo,'r')
% [gain ,wcu] = wcgain(F.Lo)
% figure(1)
% sigma(error_based_linear_car_model,{0.01 2000})
% figure(2)
% bodemag(F_loopsens.To,{1 10000})
%% RUN SIMULATIONS
% Simulation parameters.
clc
clf
close all
w = warning ('off','all');
sim_init_linear_model = [0.6,0,0,track(3,1),0]; %[xdot y ydot yaw yawdot]
carInitialPos = [track(1,1),track(2,1)];
sim_time = 200;

dt = 0.001;
plotStep = 80;
save('data');

runSimulations(carInitialPos,dt,plotStep);
load('results.mat')
