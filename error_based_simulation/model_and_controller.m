%% CREATE VEHICLE MODEL
clear
clc
clf
close all

m   =  ureal('m',0.207,'percent',eps);  %    Vehicle mass. [kg]
a   =  ureal('a',0.0495,'percent',eps);  %    Distance from front axle to COG. [m]
b   =  ureal('b',0.052,'percent',eps);  %    Distance from rear axle to COG. [m]
L   =  a+b;  %    Distance between rear and front axle. [m]
L.Name = 'L';
mu = 0.5; % Friction coefficient
Iz  = (1/12)*m*((L*1.1)^2 + 0.06^2);       %  Rotational inertia Z axis.
Iz.Name = 'Iz';
rfw = 0.022/2;      % Radius front wheel [m]
rrw = 0.027/2;      % Radius rear wheel [m]
mrw = 0.003;        % Mass rear wheel
mfw = 0.0025;       % Mass front wheel
Iwr = (1/2)*mrw^2;  % Rotational inertia wheel.
Iwf = (1/2)*mfw^2;

 % Lateral tire stiffness.  [NO FUCKING CLUE]
Cyr = m.NominalValue*50;
Cyr = ureal('Cyr',Cyr,'percent',eps);
stiffnessRatio = ureal('stiffnessRatio',1.12,'percent',eps);
Cyf = Cyr.NominalValue*stiffnessRatio;%ureal('Cyf',Cyr,'percent',20);%Cyr*stiffnessRatio;%
Cyf.Name = 'Cyf';
Ck = (1-(a/L))*m  / (2*Cyf*30)   ; % Steering angle to slip ratio factor (super made up)
Cd  = 0.8;          % Air resistance coefficient. [Unitless (0,1)]
p   = 1.184;        % Density of air [kg/dm^3]
Af  = 0.002175;     % Frontal areal of vehicle [m^2]
Vx_lin = 5;       % Speed at which we linearize around [m/s]
delay = 20;       % delay for steering actuator

% A matrix for state vector [e1 e1dot e2 e2dot]
A = [
           0   1                               0                       0
           0    -(2*Cyf+2*Cyr)/(Vx_lin*m)     (2*Cyf+2*Cyr)/m         -((2*Cyf*a-2*Cyr*b))/(Vx_lin*m)
           0    0                               0                       1
           0    -(2*Cyf*a-2*Cyr*b)/(Iz*Vx_lin)  (2*Cyf*a-2*Cyr*b)/Iz    -(2*(a^2)*Cyf+2*(b^2)*Cyr)/(Iz*Vx_lin)
    ];
A_aug = [0 1 0 0 0;
        [zeros(4,1) A]];

% B matrix for state vector [e1 e1dot e2 e2dot]
B_1 = [
    0               
    2*Ck*Cyf/m      
    0               
    (2*Cyf*a/Iz)    
    ];
B_1_aug = [0; B_1];

B_2 = [
    0
    (-(2*Cyf*a-2*Cyr*b)/(m*Vx_lin)-Vx_lin)
    0
    -(2*Cyf*a^2+2*Cyr*b^2)/(Iz*Vx_lin)
    ];
B_2_aug = [0; B_2];

B = [B_2,B_1];

B_aug = [B_2_aug, B_1_aug];

C = eye(4);

C_aug = eye(5);

D = zeros(4,1);

D_aug = zeros(5,1);


% Create the state space models with proper names and stuff
error_based_linear_car_model = ss(A, ...
    B_1, C, D);

error_based_linear_car_model_aug = ss(A_aug, ...
    B_1_aug, C_aug, D_aug);

complete_sys = ss(A,B,C,[D,D]);

error_based_linear_car_model.InputName = {'\delta'};
error_based_linear_car_model.OutputName = {'e_1','\dot{e}_1','e_2','\dot{e}_2'};

% CREATE CONTROLLER
% LQR controller matrices
Q = diag([1 1 1 1]);
Q_aug = diag([1,1,1,1,1]);
R = [1];
R_aug = 1;
[K,~,~] = lqr(error_based_linear_car_model.NominalValue,Q,R);
[K_aug,~,~] = lqr(error_based_linear_car_model_aug.NominalValue,Q_aug,R_aug);
K_2input = [zeros(1,4);K];

F = loopsens(error_based_linear_car_model_aug, K_aug);


dt = 1e-3;

%[wcg,wcu,info] = wcgain(F.PSi)
%%
clc

s = tf('s');
SteerDelay = 1/(s/delay +1);

S = (1/s)*eye(4); %intergrator
systemnames = 'B_1 B_2 A SteerDelay K S'; 
inputvar = '[des_yawrate]';
input_to_B_2 = '[des_yawrate]';
outputvar = '[S]';
input_to_SteerDelay = '[-K]';
input_to_S = '[B_1 + B_2 + A]';
input_to_A = '[S]';
input_to_K = '[S]';
input_to_B_1 = '[SteerDelay]';


cleanupsysic = 'yes';
T = sysic; 
exist('inputvar') 

%bodemag(T)
%[wcg,wcu,info] = wcgain(T)

%%
% Plot the error states together with the reference yawrate (track) and
% steering angle.
for i = 1: 10
sim('simulink_model')
hold on
plot(simout)
end
leg1 = legend('$e_1$','$\dot{e}_1$','$e_2$','$\dot{e}_2$','$\dot{\psi}_{des}$','$\delta$')
set(leg1, 'Interpreter','Latex')
