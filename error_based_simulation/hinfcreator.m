B_1 = B_1.NominalValue;
B_2 = B_2.NominalValue;
A = A.NominalValue;
%%
clc
s = tf('s');
Steer_Delay = 1/(s/5 +1);
W_phiDot_des = ((s/7.9577  + 1)) / (s/4.5 + 1);
W_phiDot_des = ss(W_phiDot_des);

w_b_1 = (s/46 + 1/7) / ((s/7.9577  + 50));
W_B_1 = blkdiag(w_b_1, w_b_1, w_b_1, w_b_1);
W_B_1 = ss(W_B_1);

w_b_2 = (s/46 + 1/7) / ((s/7.9577  + 50));
W_B_2 = blkdiag(w_b_2, w_b_2, w_b_2, w_b_2);
W_B_2 = ss(W_B_2);

w_n_1 = 0.1;
w_n_2 = 0.2;
w_n_3 = 0.09*pi/180;
w_n_4 = 0.15*pi/180;
W_N = blkdiag(w_n_1, w_n_2, w_n_3, w_n_4);
W_N = ss(W_N);

w_perf = 1/w_b_2;
W_PERF = blkdiag(10*w_perf,1, w_perf, w_perf);
W_PERF = ss(W_PERF);

W_S = blkdiag(1/w_perf, 1/w_perf, 1/w_perf, 1/w_perf);
W_S = ss(W_S);


S = blkdiag((1/s),(1/s),(1/s),(1/s)); %intergrator
S = ss(S);

% systemnames = 'W_phiDot_des W_B_1 W_B_2 B_1 B_2 A S W_S W_N W_PERF'; 
% inputvar = '[des_yawrate{1}; noise{4}; delta{1}]';
% outputvar = '[W_PERF; W_S+S+W_N]';
% 
% input_to_W_phiDot_des = '[des_yawrate]';
% input_to_B_2 = '[W_phiDot_des]';
% input_to_W_B_2 = '[B_2]';
% %input_to_Steer_Delay = '[delta]';
% input_to_B_1 = '[delta]'; %input_to_B_1 = '[Steer_Delay]';
% input_to_W_B_1 = '[B_1]';
% input_to_S = '[W_B_1 + B_1 + W_B_2 + B_2 + A]';
% input_to_W_S = '[S]';
% input_to_A = '[S]';
% input_to_W_PERF = '[S+W_S]';
% input_to_W_N = '[noise]';

systemnames = 'W_phiDot_des W_B_1 W_B_2 B_1 B_2 A S W_S W_N W_PERF'; 
inputvar = '[unc1{4};unc2{4};unc3{4}; des_yawrate; noise{4}; delta{1}]';
outputvar = '[W_B_2; W_B_1; W_S; W_PERF; unc3+S+W_N]';

input_to_W_phiDot_des = '[des_yawrate]';
input_to_B_2 = '[W_phiDot_des]';
input_to_W_B_2 = '[B_2]';
%input_to_Steer_Delay = '[delta]';
input_to_B_1 = '[delta]'; %input_to_B_1 = '[Steer_Delay]';
input_to_W_B_1 = '[B_1]';
input_to_S = '[unc1 + B_1 + unc2 + B_2 + A]';
input_to_W_S = '[S]';
input_to_A = '[S]';
input_to_W_PERF = '[S+unc3]';
input_to_W_N = '[noise]';


cleanupsysic = 'yes';
T = sysic; 


% hold on
%sigma(T,'g')

nmeas = 4;
ncont = 1;
[Chinf, CL, GammaHinf, info] = hinfsyn(T,nmeas,ncont,'DISPLAY','ON', 'METHOD', 'lmi','TOLGAM',0.1);

[ah,bh,ch,dh] = ssdata(Chinf);
rcond(ah)