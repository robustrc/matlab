clear all
clf
close all
clc
a = 0;
l = 0.1;
x = 0:0.1:3;
for theta = 0:5:20
theta = theta*180/pi;
hold on
plot(x,-a-theta*l ./ x.^2,'r')
plot(x,((-a - theta* l) + 2*(x - 1) *(a + theta*l)- 3* (x - 1).^2 *(a + theta* l)),'b')
end