clear all
clf
close all
clc
% Vehicle model

m   =  0.195;  %    Vehicle mass. [kg]
a   =  0.051;  %    Distance from front axle to COG. [m]
b   =  0.051;  %    Distance from rear axle to COG. [m]
L   =  a+b;  %    Distance between rear and front axle. [m]
mu = 0.5; % Friction coefficient
Iz  = (1/12)*m*(0.130^2 + 0.06^2);       %  Rotational inertia Z axis.
rfw = 0.022/2;      % Radius front wheel [m]
rrw = 0.027/2;      % Radius rear wheel [m]
mrw = 0.003;        % Mass rear wheel
mfw = 0.0025;       % Mass front wheel
Iwr = (1/2)*mrw^2;  % Rotational inertia wheel.
Iwf = (1/2)*mfw^2;

Cyr  = m*50;       % Lateral tire stiffness.  [N/rad]
Cyf = Cyr;

Ck = 0.01;           % Steering angle to slip ratio factor (super made up)
Cd  = 0.8;          % Air resistance coefficient. [Unitless (0,1)]
p   = 1.184;        % Density of air [kg/dm^3]
Af  = 0.002175;     % Frontal areal of vehicle [m^2]
Vx_lin = 1;       % Speed at which we linearize around [m/s]

% A matrix for state vector [x xdot y ydot yaw yawdot]
A = [
    0   1                        0   0                               0   0
    0   -0*400*(1/2)*p*Cd*Af     0   0                               0   0
    0   0                        0   1                               0   0
    0   0                        0   -(2*Cyf+2*Cyr)/(Vx_lin*m)       0   (-((2*Cyf*a-2*Cyr*b))/(Vx_lin*m) - Vx_lin)
    0   0                        0   0                               0   1
    0   0                        0   -(2*Cyf*a-2*Cyr*b)/(Iz*Vx_lin)  0   -(2*a^2*Cyf+2*b^2*Cyr)/(Iz*Vx_lin)
    ];
% A matrix for state vector [e1 e1dot e2 e2dot]
A_error = [
    0   1                               0                       0
    0   -(2*Cyf+2*Cyr)/(Vx_lin*m)       (2*Cyf+2*Cyr)/m         -((2*Cyf*a-2*Cyr*b))/(Vx_lin*m)
    0   0                               0                       1
    0   -(2*Cyf*a-2*Cyr*b)/(Iz*Vx_lin)  (2*Cyf*a-2*Cyr*b)/Iz    -(2*a^2*Cyf+2*b^2*Cyr)/(Iz*Vx_lin)
    ];
% B matrix for state vector [x xdot y ydot yaw yawdot]
B = [  
    0             
    0*Cyf*Ck/m      
    0               
    2*Cyf/m         
    0               
    (2*Cyf*a/Iz)    
    ];
% B matrix for state vector [e1 e1dot e2 e2dot]
B_error_1 = [
    0               
    2*Cyf/m         
    0               
    (2*Cyf*a/Iz)    
    ];
B_error_2 = [
    0
    (-(2*Cyf*a-2*Cyr*b)/(m*Vx_lin)-Vx_lin)
    0
    -(2*Cyf*a^2+2*Cyr*b^2)/(Iz*Vx_lin)
];


C = eye(6);
C_error = eye(4);
D = zeros(6,1);
D_error = zeros(4,1);

%system = ss(A,B,C,D);
system_error = ss(A_error, B_error_1, C_error, D_error);

Q = [   
    0.001   0       0       0
    0       0.1     0       0   
    0       0       0.001   0   
    0       0       0       50
    ];
R = 0.001;
sim_init_linear_model = [0,Vx_lin,0,0,0,0]; %[x xdot y ydot yaw yawdot]
sim_init_error_states = [0,0,0,1]; % [e1 e1Dot e2 e2Dot]
sim_time = 15;

[K_error,~,~] = lqr(system_error,Q,R);


sim('sim_error_based_state_space');

globalXVel = zeros(size(simout.data,1),1);
globalYVel = zeros(size(simout.data,1),1);
globalXPos = zeros(size(simout.data,1),1);
globalYPos = zeros(size(simout.data,1),1);


yawdata(:) = simout.data(:,5);%*180/pi;
for i = 1:size(simout.data,1)
    globalXVel(i) = sqrt((simout.data(i,2)^2+simout.data(i,4)^2)).*cosd(yawdata(i));
    globalYVel(i) = sqrt((simout.data(i,2)^2+simout.data(i,4)^2)).*sind(yawdata(i));
end
for i = 1:size(simout.data,1)-1
    globalXPos(i+1) = globalXPos(i) + globalXVel(i)*0.001;
    globalYPos(i+1) = globalYPos(i) + globalYVel(i)*0.001;
end

dataPoints = size(globalXPos,1);
maxX = max(globalXPos(1:dataPoints));
maxY = max(globalYPos(1:dataPoints));
minX = min(globalXPos(1:dataPoints));
minY = min(globalYPos(1:dataPoints));

close all
figure(1)

car_size_half = a;
angle = 30;
for i=1:200:dataPoints%size(globalXPos,2)
    dotx(1) = [globalXPos(i)+car_size_half*cosd(angle+yawdata(i))];
    dotx(2) = [globalXPos(i)+car_size_half*cosd(360-angle+yawdata(i))];
    dotx(3) = [globalXPos(i)+car_size_half*cosd(180+angle+yawdata(i))];
    dotx(4) = [globalXPos(i)+car_size_half*cosd(180-angle+yawdata(i))];
    dotx(5) = dotx(1);
    doty(1) = [globalYPos(i)+car_size_half*sind(angle+yawdata(i))];
    doty(2) = [globalYPos(i)+car_size_half*sind(360-angle+yawdata(i))];
    doty(3) = [globalYPos(i)+car_size_half*sind(180+angle+yawdata(i))];
    doty(4) = [globalYPos(i)+car_size_half*sind(180-angle+yawdata(i))];
    doty(5) = doty(1);
    frontx(1) = dotx(1);
    frontx(2) = dotx(2);
    fronty(1) = doty(1);
    fronty(2) = doty(2);
    
    fill(dotx,doty,'k');
    hold on
    plot(frontx,fronty,'g','Linewidth',2)
    
    %plot(globalXPos(i), globalYPos(i),'r*');
    axis([-abs(minX) maxX -abs(minY)-1 maxY+1])
    axis equal
    pause(0.01)
end
%%
alpha = 0.5:0.01:30
figure(3)
plot(alpha,a./tand(alpha))
