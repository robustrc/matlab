function output = getEuclidDistance2D(x1,x2,y1,y2)
    output = sqrt((x1-x2)^2 + (y1-y2)^2);
end
