function output = getTriangleArea(x1,x2,x3,y1,y2,y3)
    output = abs((1/2)*(x1*(y2-y3) + x2*(y3-y1) + x3*(y1-y2)));
end